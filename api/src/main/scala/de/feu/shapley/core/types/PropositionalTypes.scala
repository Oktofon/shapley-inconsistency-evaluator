/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.core.types

import scala.collection.immutable.ListMap
import scala.language.implicitConversions

/**
  * Common propositional types
  * @author Alexander Erben (a_erben@outlook.com)
  */
object PropositionalTypes {

  /** A variable is represented by an Integer, as is the case in DIMACS */
  type Variable = Int

  /** A clause is a list of variables, representing a disjunction in a formula in conjunctive normal form */
  type Clause = List[Variable]

  /** A formula is a list of clauses. This type represents a propositional formula in conjunctive normal form */
  type Formula = List[Clause]

  /** A knowledge base is a non-empty set of propositional [[Formula]] */
  type KnowledgeBase = List[Formula]

  /** The shapley value of a [[Formula]] is represented as a scala [[Double]] */
  type ShapleyValue = Double

  /** The result of a [[ShapleyValue]] calculation over a [[KnowledgeBase]] */
  type ShapleyCalculationResult = ListMap[Formula, ShapleyValue]

  implicit class KnowledgeBasedEnhancements(K: KnowledgeBase) {

    /**
      * Remove a propositional [[Formula]] from a [[KnowledgeBase]]
      * @param α to remove
      * @return the [[KnowledgeBase]] sans the given [[Formula]]
      */
    def remove(α: Formula) = {
      assert(K.nonEmpty)
      K filterNot (_.equals(α))
    }

    /**
      * Tell the number of unique variables used in this knowledge base
      * @return number of variables
      */
    def nVars: Int = {
      val flattened:List[List[Int]] = K.flatten
      flattened.flatten.map(Math.abs).toSet.size
    }
  }

  implicit class FormulaEnhancements(F: Formula) {
    /**
      * Tell the number of unique variables used in this formula
      * @return number of variables
      */
    def nVars:Int = F.flatten.map(Math.abs).toSet.size
  }

  /**
    * To transform a sequence of [[Formula]] to a single [[Formula]], combine them with an implicit AND
    * @param K the sequence of [[Formula]], must not be empty
    * @return a single [[Formula]] yielded by combine the [[Formula]] of the [[KnowledgeBase]] with an AND
    */
  implicit def buildExpressionFromSeq(K: List[Formula]): Formula = {
    assert(K.nonEmpty)
    K reduce ((e1, e2) => e1 ++ e2)
  }

  /**
    * To yield a [[KnowledgeBase]] from a single [[Formula]], create a one-element [[List]] of it
    * @param E the [[Formula]] to transform to a [[KnowledgeBase]]
    * @return a one-element [[KnowledgeBase]]
    */
  implicit def toKnowledeBase(E: Formula): KnowledgeBase = List(E)

  /**
    * Helper explicit to convert a propositional [[Formula]] to a String representation
    * @param E the [[Formula]] to convert
    * @return a String representation
    */
  implicit def formulaAsString(E: Formula): String = E.toString()

}
