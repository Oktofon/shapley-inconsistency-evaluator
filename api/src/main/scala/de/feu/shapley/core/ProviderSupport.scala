package de.feu.shapley.core

import java.io.File

import org.clapper.classutil.{ClassFinder, ClassInfo}

/**
  * Internal utility object.
  * Exports the class definitions in the api module for use with the classutil
  * [[ClassFinder]] in the other modules.
  */
private[shapley] object ProviderSupport {

  /*
    * We have to load all classes of the api project to provide access to them to the classloader that
    * classfinder spawns. Otherwise, the loaded user implementations could not resolve the api classes to which
    * they must have access.
    */
  private val pathURI = this.getClass.getProtectionDomain.getCodeSource.getLocation.toURI

  /**
    * Provides all class definitions of the api modules.
    * @return
    */
  def apiClasses: Stream[ClassInfo] = ClassFinder(Seq(new File(pathURI))).getClasses()

}
