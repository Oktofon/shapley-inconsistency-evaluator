package de.feu.shapley.core.measurement

import de.feu.shapley.core.types.PropositionalTypes.KnowledgeBase

import scala.concurrent.Future

/**
  * Creates a measure instance bound to a knowledge base. The binder
  * performs preparations for the measure such as pre-calculating mimimal inconsistent
  * subsets.
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
trait InconsistencyMeasureModule {

  /**
    * Create a measure bound to a knowledge base.
    * @param K to bind the new measure to
    * @return calculation that yields a new measure
    */
  def bind(K: KnowledgeBase): Future[BoundInconsistencyMeasure]

  /**
    * Tell an identifiying key for this module. By default, this is the canonical class name.
    * @return identifier
    */
  def identify: String = getClass.getCanonicalName

  override def toString = identify
}
