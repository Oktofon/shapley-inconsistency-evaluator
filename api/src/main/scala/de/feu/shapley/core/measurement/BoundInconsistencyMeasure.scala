/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.core.measurement

import de.feu.shapley.core.types.PropositionalTypes.KnowledgeBase

import scala.concurrent.Future

/**
  * Inconsistency measure functions bound to knowledge bases.
  * All measure instances are bound by their corresponding module to the knowledge
  * base that is returned by the domain method.
  *
  * An inconsistency measure maps a propositional expression set to a number in the range of [0;1]
  * where 0 is yielded for most inconsistent expression and 1 for most consistent.
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
trait BoundInconsistencyMeasure {

  /**
    * The measure for the given [[KnowledgeBase]], which must be a subset of this
    * measures [[domain]]
    * @param K knowledge base to calculate for. Must be a subset of this measures [[domain]].
    * @return the measure as future calculation
    */
  def measure(K: KnowledgeBase): Future[Double]

  /**
    * The knowledge base for which this measure is valid.
    * [[measure]] must only be called for subsets for this base.
    * @return the base for which this measure is valid
    */
  def domain: KnowledgeBase

}
