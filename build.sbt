import no.vedaadata.sbtjavafx.JavaFXPlugin.JFX
import sbt.Keys._

val globalScalaVersion = "2.11.8"

lazy val coordinates = Seq(
  organization := "de.feu",
  version := "0.10.0",
  scalaVersion := globalScalaVersion
)

lazy val commonDependencies = Seq(
  libraryDependencies += "org.scala-lang" % "scala-reflect" % globalScalaVersion,
  libraryDependencies += "org.scala-lang" % "scala-library" % globalScalaVersion,
  libraryDependencies += "org.scala-lang" % "scala-compiler" % globalScalaVersion,
  libraryDependencies += "org.scala-lang" % "scala-actors" % globalScalaVersion,
  libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.1.5",
  libraryDependencies += "org.clapper" %% "classutil" % "1.0.5",
  libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.5"
)

lazy val api = (project in file("api")).
  settings(coordinates: _*).
  settings(commonDependencies: _*).
  settings(name := "shapley-inconsistency-evaluator-api", exportJars:=true)

lazy val core = (project in file("core")).
  dependsOn(api).
  settings(coordinates: _*).
  settings(commonDependencies: _*).
  settings(
    name := "shapley-inconsistency-evaluator-core",
    exportJars := true,
    libraryDependencies += "org.ow2.sat4j" % "org.ow2.sat4j.core" % "2.3.5",
    libraryDependencies += "org.parboiled" %% "parboiled" % "2.1.0",
    libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test",
    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.5" % "test"
  )


lazy val app = (project in file("app")).
  dependsOn(core).
  settings(coordinates: _*).
  settings(commonDependencies: _*).
  settings(
    name := "shapley-inconsistency-evaluator-app",
    exportJars := true,
    resolvers += "JCenter" at "http://jcenter.bintray.com/",
    libraryDependencies += "org.scalafx" %% "scalafx" % "8.0.60-R9",
    libraryDependencies += "org.yaml" % "snakeyaml" % "1.16",
    libraryDependencies += "org.fxmisc.richtext" % "richtextfx" % "0.6.10",
    libraryDependencies += "de.jensd" % "fontawesomefx" % "8.0.10",
    libraryDependencies += "org.controlsfx" % "controlsfx" % "8.40.10",
    libraryDependencies += "de.codecentric.centerdevice" % "centerdevice-nsmenufx" % "2.1.0",
    libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.1",
    libraryDependencies += "com.norbitltd" % "spoiwo" % "1.0.6"
  )

lazy val root = (project in file(".")).
  settings(coordinates: _*).
  settings(
    name := "Shapley"
  ).
  aggregate(api, core, app).
  dependsOn(api, core, app) // This is necessary to make the CLASSPATH of all modules available to the jfx build

jfxSettings

fork in run := true

JFX.nativeBundles := "all"

JFX.devKit := JFX.jdk(System.getenv("JAVA_HOME"))

JFX.addJfxrtToClasspath := true

JFX.mainClass := Some("de.feu.shapley.app.ui.Main")

JFX.vendor := "FernUniversität in Hagen"

JFX.title := "Shapley"