package de.feu.shapley.app.ui.components

import scalafx.Includes._
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.scene.layout.BorderPane
import scalafx.scene.layout.Priority.Always
import scalafx.scene.paint.Color
import scalafx.scene.web.WebView
import scalafx.stage.Stage

trait HelpTarget extends I18nSupport {

  val helpId:String

  def helpButton(): Button = {
    new Button() {
      text = "general.help".i18n()
      prefWidth = 200
      onAction = () => {
        createStage().show()
      }
    }
  }

  def helpMenu(): Menu = {
    new Menu{
      text = "general.help".i18n()
      items = List(
        new MenuItem{
          text = "general.help.open".i18n()
          onAction = () => {
            createStage().show()
          }
        }
      )
    }
  }


  private def createStage(): Stage = {
    val browser = new WebView {
      hgrow = Always
      vgrow = Always
    }
    val engine = browser.engine
    val indexPage = getClass.getClassLoader.getResource(s"help/index.html")
    val desiredHelpPage = getClass.getClassLoader.getResource(s"help/$helpId.html")
    assert(desiredHelpPage != null, s"Help resource not found: $helpId")
    engine.load(desiredHelpPage.toExternalForm)
    new Stage {
      title = "general.help".i18n()
      width = 800
      height = 600
      scene = new Scene {
        fill = Color.LightGray
        root = new BorderPane {
          hgrow = Always
          vgrow = Always
          top = new ToolBar {
            content = List(
              new Button() {
                text = "general.help.button.index".i18n()
                prefWidth = 100
                onAction = () => {
                    engine.load(indexPage.toExternalForm)
                }
              },
              new Button() {
                text = "general.help.button.back".i18n()
                prefWidth = 100
                onAction = () => {
                    if (engine.getHistory.getCurrentIndex > 0) {
                      engine.getHistory.go(-1)
                    }
                }
              }
            )
          }
          center = browser
        }
      }
    }
  }
}
