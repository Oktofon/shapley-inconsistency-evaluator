package de.feu.shapley.app.ui.wizards

import javafx.util.StringConverter

import de.feu.shapley.app.ui.components.I18nSupport
import de.feu.shapley.core.measurement.InconsistencyMeasureModule
import de.feu.shapley.core.provider.PluginAndCoreProvider
import org.controlsfx.dialog.WizardPane

import scalafx.collections.ObservableBuffer
import scalafx.geometry.Insets
import scalafx.scene.control.{CheckBox, ComboBox, Label}
import scalafx.scene.layout.GridPane

private[wizards] class ModeSelectPane extends WizardPane with I18nSupport {
  setHeaderText("analysis.modeselect.header".i18n())
  val measures = ObservableBuffer[InconsistencyMeasureModule]()
  PluginAndCoreProvider.findAll() foreach measures.add
  val measuresLabel = new Label("analysis.modeselect.measure".i18n())
  GridPane.setConstraints(measuresLabel, 1, 1)
  val measuresCb = new ComboBox[InconsistencyMeasureModule] {
    items = measures
  }
  measuresCb.converter.set(new StringConverter[InconsistencyMeasureModule] {

    override def toString(t: InconsistencyMeasureModule): String = i18n_inconsistencyMeasure(t.identify)

    override def fromString(string: String): InconsistencyMeasureModule = measures.filter(p => p.identify == string).head
  })
  GridPane.setConstraints(measuresCb, 2, 1)
  measuresCb.getSelectionModel.selectFirst()
  val calcShap = new CheckBox("analysis.modeselect.shapley".i18n())
  GridPane.setConstraints(calcShap, 1, 2)
  val gridpane = new GridPane {
    padding = Insets(20, 20, 20, 20)
    hgap = 5
    vgap = 5
    children = Seq(measuresLabel, measuresCb, calcShap)
  }
  setContent(gridpane)
  def selectedMeasure = measuresCb.getSelectionModel.getSelectedItem

  def isShapleyCalculation = calcShap.isSelected
}
