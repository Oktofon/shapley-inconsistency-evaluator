package de.feu.shapley.app.ui

import javafx.scene.control.Menu

import de.codecentric.centerdevice.MenuToolkit
import de.feu.shapley.app.ui.components.I18nSupport
import de.feu.shapley.app.ui.stages.ProjectSelectStage
import de.feu.shapley.core.provider.PluginAndCoreProvider

import scalafx.application.JFXApp

/**
  * Application entry point
  */
object Main extends JFXApp with I18nSupport {
  PluginAndCoreProvider.findAll() // preload all inconsistency measures
  System setProperty("prism.lcdtext", "false")

  applyMainMenus()

  stage = new ProjectSelectStage

  private def applyMainMenus(): Unit = {
    val isMac = System.getProperty("os.name").toLowerCase.contains("mac")
    if (isMac) {
      val tk: MenuToolkit = MenuToolkit.toolkit()
      val defaultApplicationMenu: Menu = tk.createDefaultApplicationMenu("main.name".i18n())
      val menuItems = defaultApplicationMenu.getItems
      menuItems.get(0).setText("main.mac.about".i18n())
      menuItems.get(2).setText("main.mac.hide".i18n())
      menuItems.get(3).setText("main.mac.hideothers".i18n())
      menuItems.get(4).setText("main.mac.showall".i18n())
      menuItems.get(6).setText("main.mac.quit".i18n())
      tk.setApplicationMenu(defaultApplicationMenu)
    }
  }
}
