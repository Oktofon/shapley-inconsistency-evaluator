package de.feu.shapley.app.types

import java.util.UUID

import de.feu.shapley.app.types.ProjectElement.{ProjectElementID, ProjectElementName, UnparsedKnowledgeBase}

import scala.beans.BeanProperty

/**
 * Types and constructor methods for [[ProjectElement]]
 * @author Alexander Erben
 */
object ProjectElement {

  /**
   * ID of a project element
   */
  type ProjectElementID = String

  /**
   * An unparsed knowledge base
   */
  type UnparsedKnowledgeBase = String

  /**
   * The name of a project element
   */
  type ProjectElementName = String

  /**
   * Create a new element by name and knowledge base. Generate unique id.
   * @param name of the knowledge base
   * @param knowledgeBase to represent
   * @return project element with the specified parameters and a generated ID
   */
  def create(name: String, knowledgeBase: UnparsedKnowledgeBase) = 
    new ProjectElement(UUID.randomUUID().toString, name, knowledgeBase)
}

/**
 * Main entity for the subjects of a project, each of which encapsules a single knowledge base
 * along with a name and an unique, generated ID
 * @author Alexander Erben
 */
class ProjectElement(@BeanProperty var id: ProjectElementID = null,
                     @BeanProperty var name: ProjectElementName = null,
                     @BeanProperty var knowledgeBase: UnparsedKnowledgeBase = null) {

  /** Deserialization initializer */
  def this() = {
    this(null,null,null)
  }
}