/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.app.io

import java.io.{PrintWriter, File}
import scala.io.{Codec, Source}

/**
 * Enhances the java.io File with added functionality
 * @param file to enhance
 *
 * @author Alexander Erben (a_erben@outlook.com)
 */
class RichFile(file: File) {

  /**
   * Retrieve the content of the file
   * @return the file contents encoded in UTF-(
   */
  def text = Source.fromFile(file)(Codec.UTF8).mkString

  /**
   * Write a String to this file
   * @param s to write
   */
  def writeContent(s: String) {
    val out = new PrintWriter(file, "UTF-8")
    try {
      out.print(s)
    }
    finally {
      out.close()
    }
  }
}

/**
 * Implicit conversion companion for [[RichFile]]
 * @author Alexander Erben (a_erben@outlook.com)
 */
object RichFile {

  /**
   * Enhance a [[File]]
   * @param file to enhance. Does not need to exist.
   * @return enhanced [[RichFile]]
   */
  implicit def enrichFile(file: File): RichFile = new RichFile(file)

}

