package de.feu.shapley.app.types

import java.nio.file.Path
import java.util.UUID

import de.feu.shapley.app.types.Project.ProjectID

import scala.language.implicitConversions

/**
 * Types and constructor methods for [[Project]]
 *
 * @author Alexander Erben (a_erben@outlook.com)
 */
object Project {

  /** Id of a project */
  type ProjectID = String

  /**
   * Create a new [[Project]] with a generated id
   * @param elements of the new project
   * @return new project with generated ID
   */
  def create(elements: List[ProjectElement]) =
    new Project(UUID.randomUUID().toString, elements)

}

case class Project(var id:ProjectID, var elements:List[ProjectElement], var location:Option[Path] = None){
}


