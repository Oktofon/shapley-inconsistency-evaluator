package de.feu.shapley.app.types

import scalafx.beans.property.{BooleanProperty, StringProperty}
import scalafx.collections.ObservableBuffer

/**
  * Observation proxy for [[de.feu.shapley.app.types.ProjectElement]]
  *
  * @param delegate to proxy
  * @author Alexander Erben (a_erben@outlook.com)
  */
class ObservableProjectElement(val delegate: ProjectElement) {


  val latestChanges: ObservableBuffer[String] = ObservableBuffer()

  /**
    * Contains the current pending unsaved knowledge base
    */
  val uncommitedChangesProperty = StringProperty(delegate.knowledgeBase)

  /**
    * Indicates presence of unsaved changes to the underlying knowledge base
    */
  val uncommitedChangesPresentProperty = BooleanProperty(value = false)

  /**
    * Contains the current id
    */
  val idProperty = StringProperty(delegate.id)

  /**
    * Contains the currently saved knowledge base value
    */
  val knowledgeBaseProperty = StringProperty(delegate.knowledgeBase)

  /**
    * Contains the name
    */
  val nameProperty = StringProperty(delegate.name)

  /** Bindings */
  uncommitedChangesProperty onChange ((_, _, newValue) => {
    uncommitedChangesPresentProperty.set(newValue != delegate.knowledgeBase)
    latestChanges.add(newValue)
  })

  idProperty onChange ((_, _, newValue) => delegate.id = newValue)

  knowledgeBaseProperty onChange ((_, _, newValue) => delegate.knowledgeBase = newValue)

  nameProperty onChange ((_, _, newValue) => delegate.name = newValue)

  /**
    * Save staged changes to the underlying knowledge base and mark this observable as unchanged
    */
  def commit() = {
    knowledgeBaseProperty set uncommitedChangesProperty.value
    uncommitedChangesPresentProperty set false
  }

  def hasUncommitedChanges: Boolean = uncommitedChangesPresentProperty.value

  def knowledgeBaseAsStringList: List[String] = {
    uncommitedChangesProperty.value.split('\n').toList
  }
}
