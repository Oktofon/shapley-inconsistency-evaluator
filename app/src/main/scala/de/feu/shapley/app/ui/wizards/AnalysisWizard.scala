package de.feu.shapley.app.ui.wizards

import java.util.Optional
import java.util.function.Consumer
import javafx.scene.control.ButtonType

import de.feu.shapley.app.types.{ObservableProjectElement, ProjectElement}
import org.controlsfx.dialog.Wizard.Flow
import org.controlsfx.dialog.{Wizard, WizardPane}

import scalafx.application.JFXApp

class AnalysisWizard(element: ObservableProjectElement) extends Wizard {
  val modeSelectPane = new ModeSelectPane
  setFlow(
    new Flow {
      override def advance(currentPage: WizardPane): Optional[WizardPane] = {
        currentPage match {
          case null => Optional.of(modeSelectPane)
          case modeSelectPage if currentPage == modeSelectPane => Optional.of(
            if (modeSelectPane.isShapleyCalculation)
              new ShapleyValueResultPage(element, modeSelectPane.selectedMeasure)
            else
              new InconsistencyMeasureResultPage(element, modeSelectPane.selectedMeasure)
          )
          case _ => Optional.empty()
        }
      }

      override def canAdvance(currentPage: WizardPane): Boolean = {
        currentPage == modeSelectPane
      }
    }
  )
}

object AnalysisWizard extends JFXApp {
  val element = new ProjectElement("id", "name", "a * b")
  val wizard = new AnalysisWizard(new ObservableProjectElement(element))
  wizard.showAndWait().ifPresent(new Consumer[ButtonType] {
    override def accept(t: ButtonType): Unit = System.exit(0)
  })
}

