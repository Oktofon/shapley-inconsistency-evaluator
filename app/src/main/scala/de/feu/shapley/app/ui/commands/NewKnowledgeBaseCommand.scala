package de.feu.shapley.app.ui.commands

import de.feu.shapley.app.types.{Observe, ProjectElement}
import de.feu.shapley.app.ui.stages.ProjectStage

/**
  * Issued when a new knowledge base is to be created
  */
class NewKnowledgeBaseCommand(name: String)(implicit val stage: ProjectStage) extends Command {

  private val created = Observe(ProjectElement.create(name, ""))
  private val previousSelection = stage.projectOverviewTree.getSelectionModel.getSelectedIndex

  override def apply() = {
    synchronized {
      stage.currentElements.add(created)
      stage.projectOverviewTree.getSelectionModel.selectLast()
    }
  }

  override def revert() = {
    synchronized {
      stage.currentElements.remove(created)
      stage.projectOverviewTree.getSelectionModel.select(previousSelection)
    }
  }

}
