package de.feu.shapley.app.ui.components

import scalafx.beans.binding.BooleanBinding
import scalafx.geometry.Insets
import scalafx.scene.control.Label
import scalafx.scene.layout.VBox

trait PleaseWait extends I18nSupport {

  val waitingProperty:BooleanBinding

  lazy val pleaseWaitPane = new VBox {
    padding = Insets(10, 10, 10, 10)
    visible bind waitingProperty
    children = List(
      new Label {
        text = "analysis.general.pleasewait.label".i18n()
      }
    )
  }
}
