package de.feu.shapley.app.types

/**
  * An observation proxy for project related entities
 *
 * @author Alexander Erben (a_erben@outlook.com)
 */
object Observe {

  /**
   * Proxy a [[ProjectElement]]
    *
    * @param delegate to proxy
   * @return proxy
   */
  def apply(delegate:ProjectElement) = new ObservableProjectElement(delegate)

  /**
   * Proxy a [[Project]]
   * @param delegate to proxy
   * @return proxy
   */
  def apply(delegate: Project) = new ObservableProject(delegate)
}
