package de.feu.shapley.app.ui

import java.util

import de.feu.shapley.app.ui.commands.Command

import scalafx.beans.property.BooleanProperty
;

/**
  * A stack that has a limited size. Has the usual FIFO-stack-semantics.
  *
  * @author Alexander Erben
  */
class RestrictedStack(val size: Int) {
  /**
    * Delegate observable list implementation
    */
  private val delegate: util.LinkedList[Command] = new util.LinkedList()

  val nonEmptyProperty:BooleanProperty = BooleanProperty(false)
  /**
    * Push a new command on the stack. Removes the last element from the stack if it is full.
    *
    * @param command to push
    */
  def push(command: Command) = {
    if (this.delegate.size() == this.size) {
      this.delegate.removeLast()
    }
    this.delegate.addFirst(command)
    nonEmptyProperty set true
  }

  /**
    * Pops the first element off the stack, or returns none if the stack is empty.
    *
    * @return unit
    */
  def pop(): Option[Command] = {
    if (this.delegate.isEmpty) {
      None
    } else {
      val first = this.delegate.removeFirst()
      if (this.delegate.isEmpty) nonEmptyProperty set false
      Some(first)
    }
  }

  /**
    * Clear the stack.
    */
  def clear() = {
    this.delegate.clear()
    nonEmptyProperty set false
  }

}