package de.feu.shapley.app.ui.wizards


import de.feu.shapley.app.ui.components.I18nSupport

import scalafx.beans.property.BooleanProperty
import scalafx.collections.ObservableBuffer
import scalafx.geometry.Insets
import scalafx.scene.control.Label
import scalafx.scene.layout.VBox

private[wizards] trait ErrorHandling extends I18nSupport {

  val errorLines = new ObservableBuffer[Int]()
  val errorWhileCalculation = BooleanProperty(value = false)
  errorLines onChange ((buffer:ObservableBuffer[Int], _) => errorWhileCalculation set buffer.nonEmpty)

  lazy val errorHandler = (errors: WorkerError) => {
    errors.failures.map(expression => expression._2).foreach(index => errorLines.add(index))
  }

  lazy val errorPane = new VBox {
    padding = Insets(10, 10, 10, 10)
    visible bind errorWhileCalculation
    children = List(
      new Label {
        errorWhileCalculation onChange {
          text set "analysis.general.error.result".i18n(errorLines.mkString(", "))
        }
      }
    )
  }
}
