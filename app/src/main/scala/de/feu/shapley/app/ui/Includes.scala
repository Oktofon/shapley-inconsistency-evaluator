package de.feu.shapley.app.ui

import javafx.event.{Event, EventHandler}

import scala.language.implicitConversions

/**
 * Common helper includes
 */
object Includes {
  implicit def eventHandler[T <: Event](f: T => Unit): EventHandler[T] = new EventHandler[T] {
    def handle(t: T) {
      f(t)
    }
  }
}
