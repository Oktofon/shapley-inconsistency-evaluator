package de.feu.shapley.app.ui.components

import de.feu.shapley.app.io.ProjectStorageFunctions
import de.feu.shapley.app.types.ObservableProject

import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.{Alert, ButtonType}
import scalafx.stage.{FileChooser, Stage}

/**
  * Used to save a [[ObservableProject]].
  * If uncommitted changes are present, the user can decide whether to ignore them or commit them.
  * Then, a file chooser is used to select the save location if the user selected save as or
  * save on a new project. Then, the project is saved.
  */
object ApplyChangesDialog extends I18nSupport {

  def apply(stage: Stage, project: ObservableProject, fileChooser: FileChooser, saveAs: Boolean) = {
    val ignore = new ButtonType("mainmenu.project.save.commitwarning.ignore".i18n())
    val commit = new ButtonType("mainmenu.project.save.commitwarning.commit".i18n())
    var saveWanted = true
    if (project.hasUncommitedChanges) {
      new Alert(AlertType.Confirmation) {
        initOwner(stage)
        title = "mainmenu.project.save.commitwarning.title".i18n()
        headerText = "mainmenu.project.save.commitwarning.header".i18n()
        contentText = "mainmenu.project.save.commitwarning.content".i18n()
        buttonTypes = Seq(ignore, commit, ButtonType.Cancel)
      }.showAndWait() foreach {
        case `ignore` =>
        case `commit` => project.commitAll()
        case _ => saveWanted = false
      }
    }
    if (saveWanted) {
      if (saveAs || project.locationProperty.value.isEmpty) {
        val file = Option(fileChooser.showSaveDialog(stage)) map (_.toPath)
        project.locationProperty.set(file)
      }
      if (project.delegate.location.isDefined) {
        ProjectStorageFunctions.storeProject(project.delegate)
      }
    }
  }

}
