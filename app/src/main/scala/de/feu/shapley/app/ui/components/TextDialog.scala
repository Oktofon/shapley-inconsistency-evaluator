package de.feu.shapley.app.ui.components

import scalafx.scene.control.TextInputDialog

/**
  * A text input dialog component
  */
object TextDialog {
  def apply(startString:String, title:String, header:String, contentText:String) = {
    val dialog = new TextInputDialog(startString)
    dialog.setTitle(title)
    dialog.setHeaderText(header)
    dialog.setContentText(contentText)
    dialog
  }
}
