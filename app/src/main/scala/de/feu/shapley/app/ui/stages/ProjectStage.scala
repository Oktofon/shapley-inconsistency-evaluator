/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.app.ui.stages

import java.awt.Desktop
import java.nio.file.Path
import javafx.event.ActionEvent
import javafx.stage.WindowEvent

import com.typesafe.scalalogging.Logger
import de.feu.shapley.app.io.ProjectStorageFunctions
import de.feu.shapley.app.types._
import de.feu.shapley.app.ui.Includes._
import de.feu.shapley.app.ui.commands._
import de.feu.shapley.app.ui.components._
import de.feu.shapley.core.provider
import org.slf4j.LoggerFactory

import scala.collection.mutable.ListBuffer
import scalafx.Includes._
import scalafx.beans.property.{BooleanProperty, ObjectProperty, StringProperty}
import scalafx.collections.ObservableBuffer
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.TableColumn._
import scalafx.scene.control.TableView._
import scalafx.scene.control._
import scalafx.scene.input.KeyCombination
import scalafx.scene.layout.{BorderPane, HBox, Priority, VBox}
import scalafx.stage.FileChooser.ExtensionFilter
import scalafx.stage.{FileChooser, Stage}

/**
  * Main UI. Holds the knowledge base overview tree, the top menu and the
  * [[EditorPane]]
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
object ProjectStage {

  private val openStages: ListBuffer[ProjectStage] = ListBuffer()

  private val allClosedEvents: ListBuffer[() => Unit] = ListBuffer()

  /**
    * Registers an event that should happen when all stages have been closed
    */
  def registerAllClosedEvent(event: () => Unit) {
    allClosedEvents += event
  }

  /**
    * Create a new empty [[ProjectStage]]
    *
    * @return stage
    */
  def newEmpty(): ProjectStage = {
    val created = createStage(ProjectStorageFunctions.nilProject)
    openStages += created
    created
  }

  /**
    * Create a [[ProjectStage]] from a [[Project]]
    *
    * @param project to load
    * @return loaded stage
    */
  def newFromElements(project: Project): ProjectStage = {
    val created = createStage(project)
    openStages += created
    created
  }

  /**
    * Create a [[ProjectStage]] with default elements
    *
    * @return stage with default elements
    */
  def newWithDefaultElements(): ProjectStage = {
    val created = createStage(ProjectStorageFunctions.defaultProject)
    openStages += created
    created
  }

  /**
    * Create a new stage. If another stage is already open, the new
    * stage will have an initial x and y offset so that it does not
    * hide the previous stage
    *
    * @param project used for the new stage
    * @return opened stage
    */
  private def createStage(project: Project): ProjectStage = {
    this.synchronized {
      val first = openStages.isEmpty
      val created = new ProjectStage(Observe(project)) {
        onHidden = (_: WindowEvent) => {
          val index: Int = openStages indexOf this
          openStages.remove(index, 1)
          if (openStages.isEmpty) {
            allClosedEvents foreach (_.apply())
          }
        }
      }
      if (!first) {
        val lastX = openStages.head.getX
        val lastY = openStages.head.getY
        created.setX(lastX + 20)
        created.setY(lastY + 20)
      }
      created
    }
  }
}

class ProjectStage(project: ObservableProject) extends Stage with I18nSupport with HelpTarget with UndoRedoTarget {

  override val helpId = "main-window"

  implicit val self = this // make own instance available as implicit parameter

  private val logger = Logger(LoggerFactory.getLogger(ProjectStage.getClass))

  /** current project elements visible in the UI */
  val currentElements: ObservableBuffer[ObservableProjectElement] = project.elementsProperty
  currentElements.onChange((buffer, _) => {
    elementsEmpty set buffer.isEmpty
  })

  /** property that indicates if no elements are present in the current project */
  val elementsEmpty = BooleanProperty(currentElements.isEmpty)

  /** property that is bound to the currently selected element in the list view */
  val currentElement: ObjectProperty[ObservableProjectElement] = new ObjectProperty[ObservableProjectElement]
  currentElement set Observe(ProjectStorageFunctions.nilProjectElement)
  /** holds the toolbar and editor */
  private val editorPane = new EditorPane(currentElement, elementsEmpty)

  /** used to open and save a project */
  private val fileChooser = new FileChooser() {
    extensionFilters.add(new ExtensionFilter("project.extensionfilter".i18n(), "*.siv"))
    initialFileName = "Projekt.siv"
  }

  /** overview of all available project elements */
  val projectOverviewTree: TableView[ObservableProjectElement] =
    new TableView[ObservableProjectElement] {
      minWidth = 250
      maxWidth = 250
      placeholder = new Label("project.empty".i18n())
      selectionModel().setSelectionMode(SelectionMode.SINGLE)
      items.bind(ObjectProperty(currentElements))
      columns ++= List(
        new TableColumn[ObservableProjectElement, String]() {
          prefWidth = 248
          prefHeight = 248
          resizable set false
          text = "mainwindow.knowledgebase.label".i18n()
          cellValueFactory = { (that: TableColumn.CellDataFeatures[ObservableProjectElement, String]) => {
            val value = if (that.value.uncommitedChangesPresentProperty.value)
              that.value.nameProperty.value + "project.changed".i18n()
            else that.value.nameProperty.value
            new StringProperty(value)
          }
          }
        })
      currentElement <== selectionModel().selectedItem
    }

  projectOverviewTree.setStyle("-fx-background-color: -fx-outer-border, -fx-inner-border, -fx-body-color; -fx-background-insets: 0, 1, 2; -fx-background-radius: 5, 4, 3;")

  // Refresh the table column when the names or changed state
  // of the knowledge bases change
  currentElement.onChange((_, _, newValue: ObservableProjectElement) => {
    if (newValue != null) {
      newValue.uncommitedChangesPresentProperty onChange {
        projectOverviewTree.getColumns.head.setVisible(false)
        projectOverviewTree.getColumns.head.setVisible(true)
      }
      newValue.nameProperty onChange {
        projectOverviewTree.getColumns.head.setVisible(false)
        projectOverviewTree.getColumns.head.setVisible(true)
      }
    }
    ()
  })

  /** The project menu in the top menu bar */
  private val projectMenu = new Menu() {
    text = "project.label".i18n()
    items = List(
      new MenuItem() {
        text = "mainmenu.project.createnew".i18n()
        onAction = (ev: ActionEvent) => {
          ProjectStage.newEmpty().show()
        }
      },
      new MenuItem() {
        text = "mainmenu.project.save".i18n()
        onAction = (ev: ActionEvent) => {
          ApplyChangesDialog.apply(self, project, fileChooser, saveAs = false)
        }
      },
      new MenuItem() {
        text = "mainmenu.project.saveas".i18n()
        onAction = (ev: ActionEvent) => {
          ApplyChangesDialog.apply(self, project, fileChooser, saveAs = true)
        }
      },
      new MenuItem() {
        text = "mainmenu.project.load".i18n()
        onAction = (event: ActionEvent) => {
          val file = fileChooser showOpenDialog ProjectStage.this
          if (file != null) ProjectStage.newFromElements(ProjectStorageFunctions loadProject file).show()
        }
      },
      new MenuItem() {
        text = "mainmenu.project.openplugindir".i18n()
        onAction = (event: ActionEvent) => {
          val path = provider.pluginPath.toFile
          Desktop.getDesktop.open(path)
          ()
        }
      },
      new MenuItem() {
        text = "mainmenu.project.close".i18n()
        onAction = (event: ActionEvent) => {
          self.close()
        }
      })
  }

  /** The edit menu in the top menu bar */
  private val editMenu = new Menu() {
    text = "mainwindow.edit".i18n()
    items = List(
      new MenuItem() {
        text = "mainmenu.knowledgebase.new.title".i18n()
        accelerator = KeyCombination.keyCombination("CTRL +N")
        onAction = (event: ActionEvent) => {
          TextDialog(
            startString = "",
            title = "mainmenu.knowledgebase.new.title".i18n(),
            header = "mainmenu.knowledgebase.new.text".i18n(),
            contentText = "mainmenu.knowledgebase.new.name".i18n()
          ).showAndWait() match {
            case Some(name) if name != "" =>
              execute(new NewKnowledgeBaseCommand(name))
            case _ =>
          }
        }

      },
      new MenuItem() {
        text = "mainmenu.knowledgebase.rename.title".i18n()
        onAction = (event: ActionEvent) => {
          TextDialog(
            startString = "",
            title = "mainmenu.knowledgebase.rename.title".i18n(),
            header = "mainmenu.knowledgebase.rename.text".i18n(),
            contentText = "mainmenu.knowledgebase.rename.newname".i18n()
          ).showAndWait() match {
            case Some(name) if name != "" =>
              execute(new RenameKnowledgeBaseCommand(name))
            case _ =>
          }
        }
        accelerator = KeyCombination.keyCombination("CTRL +R")
      },
      new MenuItem() {
        text = "mainmenu.knowledgebase.delete.title".i18n()
        onAction = (event: ActionEvent) => {
          new Alert(AlertType.Confirmation) {
            initOwner(owner)
            title = "mainmenu.knowledgebase.delete.title".i18n()
            headerText = "mainmenu.knowledgebase.delete.header".i18n()
            contentText = "mainmenu.knowledgebase.delete.text".i18n()
          }.showAndWait() match {
            case Some(buttonType) if buttonType == ButtonType.OK =>
              execute(new DeleteKnowledgeBaseCommand())
            case _ =>
          }
          logger.debug("Deleted current knowledge base")
        }
        accelerator = KeyCombination.keyCombination("CTRL +D")
      }, new MenuItem() {
        text = "mainmenu.edit.undo".i18n()
        onAction = (a: ActionEvent) => undo()
        accelerator = KeyCombination.keyCombination("CTRL +Z")
      }, new MenuItem() {
        text = "mainmenu.edit.redo".i18n()
        onAction = (a: ActionEvent) => redo()
        accelerator = KeyCombination.keyCombination("CTRL +SHIFT +Z")
      })
  }


  private def applyLocationTitle = (optPath: Option[Path]) => optPath match {
    case None => title set "project.label.new".i18n
    case Some(location) => title set location.toString
  }

  applyLocationTitle(project.locationProperty.value)
  project.locationProperty onChange ((_, _, newValue) => {
    applyLocationTitle(newValue)
  })

  /** Apply the scene */
  scene = new Scene {
    stylesheets.add(ProjectStage.getClass.getClassLoader.getResource("css/keywords.css").toExternalForm)
    minWidth = 700
    minHeight = 300
    maxHeight = Double.MaxValue
    root =
      /* This pane serves as container for all other UI components, including the menu*/
      new BorderPane {
        id = "pane"
        vgrow = Priority.Always
        maxHeight = Double.MaxValue
        top =
          new VBox {
            vgrow = Priority.Never
            children = new MenuBar {
              useSystemMenuBar = false
              menus = List(projectMenu, editMenu, helpMenu())
            }
          }
        center =
          /* This box holds all UI components except the top menu */
          new HBox {
            maxHeight = Double.MaxValue
            vgrow = Priority.Always
            children = List(
              projectOverviewTree,
              editorPane
            )
          }
      }
  }

  // Select the first knowledge base
  projectOverviewTree.getSelectionModel.selectFirst()
}


