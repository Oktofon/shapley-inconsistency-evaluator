package de.feu.shapley.app.ui.stages

import de.feu.shapley.app.types.ObservableProjectElement
import de.feu.shapley.app.ui.components.{I18nSupport, PleaseWait}
import de.feu.shapley.core.parsers.{FormulaParser, ParseFailure, ParseResult, ParseSuccess}

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scalafx.Includes._
import scalafx.beans.binding.BooleanBinding
import scalafx.beans.property._
import scalafx.collections.ObservableBuffer
import scalafx.concurrent.Task
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control.TableColumn._
import scalafx.scene.control._
import scalafx.scene.layout.{BorderPane, HBox, StackPane, VBox}
import scalafx.scene.text.{Font, Text}
import scalafx.stage.Stage

class SyntaxCheckStage(element: ObservableProjectElement) extends Stage with PleaseWait {

  private val results = new ObservableBuffer[SyntaxCheckResultRow]()
  private val resultsAvailable = BooleanProperty(value = false)
  val waitingProperty: BooleanBinding = resultsAvailable.not

  results onChange ((buffer, _) => resultsAvailable set buffer.nonEmpty)


  private val resultTable: HBox = new HBox {
    padding = Insets(10, 10, 10, 10)
    visible bind resultsAvailable
    children = new TableView[SyntaxCheckResultRow](results) {
      style = "-fx-focus-color: grey;"
      columns ++= List(new TableColumn[SyntaxCheckResultRow, String] {
        sortable = false
        text = "syntaxcheck.table.column.expression".i18n()
        cellValueFactory = _.value.propositionalExpression
        prefWidth = 400
      }, new TableColumn[SyntaxCheckResultRow, String] {
        sortable = false
        text = "syntaxcheck.table.column.result".i18n()
        cellValueFactory = _.value.result
        prefWidth = 180
      })
    }
  }

  private val pane: BorderPane = new BorderPane {
    resizable = false
    top = new HBox {
      padding = Insets(10, 10, 10, 10)
      children = new Text {
        text = "syntaxcheck.title".i18n(element.nameProperty.value)
        font = new Font("sans-serif", 16)
      }
    }

    center = new StackPane {
      children = List(
        resultTable,
        pleaseWaitPane
      )
    }
  }

  scene = new Scene {
    content = new VBox {
      title = "syntaxcheck.window.title".i18n()
      padding = Insets(20, 20, 20, 20)
      spacing = 10
      children = List(
        pane
      )
    }
  }

  runWorker(element)

  class SyntaxCheckResultRow(expression: String, resultMessage: String) {

    val propositionalExpression = new StringProperty(expression)

    val result = new StringProperty(resultMessage)
  }

  def runWorker(element: ObservableProjectElement) = {
    object Worker extends Task(new javafx.concurrent.Task[Unit] with I18nSupport {

      override def call(): Unit = {
        val expressionsAsString = element.uncommitedChangesProperty.value.split('\n').toList
        FormulaParser.parseAll(expressionsAsString).map(exprs => {
          exprs.foreach {
            case ParseSuccess(_, source, _) =>
              val row = new SyntaxCheckResultRow(source, "syntaxcheck.table.content.valid".i18n())
              results.add(row)
            case ParseFailure(reason, source, _) =>
              val row = new SyntaxCheckResultRow(source, "syntaxcheck.table.content.invalid".i18n())
              results.add(row)
          }
        })
      }

      private def containsError(exprs: List[ParseResult]): Boolean = exprs.exists(_.isInstanceOf[ParseFailure])
    })
    Worker.run()
  }


}
