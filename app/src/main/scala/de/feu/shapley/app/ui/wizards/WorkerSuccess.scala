package de.feu.shapley.app.ui.wizards

import java.time.Duration

import de.feu.shapley.core.parsers.{ParseFailure, ParseSuccess}

private[wizards] sealed trait WorkerResult
case class WorkerSuccess(results: List[ParseSuccess], time:Duration) extends WorkerResult
case class WorkerError(failures: List[(ParseFailure, Int)], time:Duration) extends WorkerResult
