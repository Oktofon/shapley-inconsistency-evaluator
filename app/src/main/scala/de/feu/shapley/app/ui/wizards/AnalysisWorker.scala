package de.feu.shapley.app.ui.wizards

import java.time.Duration

import de.feu.shapley.app.types.ObservableProjectElement
import de.feu.shapley.core.parsers.{FormulaParser, ParseFailure, ParseResult, ParseSuccess}

import scala.concurrent.ExecutionContext.Implicits.global
import scalafx.concurrent.Task

private[wizards] trait AnalysisWorker {

  val successHandler:(WorkerSuccess => Unit)
  val errorHandler:(WorkerError => Unit)

  def runWorker(element:ObservableProjectElement) = {
    object Worker extends Task(new javafx.concurrent.Task[Unit] {

      override def call(): Unit = {
        val startNano = System.nanoTime()
        val expressionsAsString = element.knowledgeBaseAsStringList

        FormulaParser.parseAll(expressionsAsString).map(exprs => {
          val parseDuration = Duration.ofNanos(System.nanoTime() - startNano)
          if (containsError(exprs)) {
            val indexedFailures: List[(ParseFailure, Int)] = exprs.map(_.asInstanceOf[ParseFailure]).zipWithIndex
            errorHandler(WorkerError(indexedFailures, parseDuration))
          }
          else {
            val successes: List[ParseSuccess] = exprs.map(expr => expr.asInstanceOf[ParseSuccess])
            successHandler(WorkerSuccess(successes, parseDuration))
          }
        })
      }
      private def containsError(exprs: List[ParseResult]): Boolean = exprs.exists(_.isInstanceOf[ParseFailure])
    })
    Worker.run()
  }

}
