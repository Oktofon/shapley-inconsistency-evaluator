package de.feu.shapley.app.ui.commands

/**
  * A unit of work for use in [[UndoRedoTarget]]
  */
abstract class Command() {
  /**
    * Apply the command. This happens once when it is performed for the first time, and then always when
    * [[UndoRedoTarget.undo()]] is called.
    */
  def apply()

  /**
    * Revert the change performed in [[Command.apply()]]. Used in [[UndoRedoTarget.redo()]]
    */
  def revert()
}