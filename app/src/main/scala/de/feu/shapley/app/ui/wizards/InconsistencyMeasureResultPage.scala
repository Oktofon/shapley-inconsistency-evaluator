package de.feu.shapley.app.ui.wizards

import java.time.temporal.ChronoUnit
import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleStringProperty
import javafx.scene.text.FontWeight

import de.feu.shapley.app.types.ObservableProjectElement
import de.feu.shapley.app.ui.components.{I18nSupport, PleaseWait}
import de.feu.shapley.core.measurement.InconsistencyMeasureModule
import de.feu.shapley.core.types.PropositionalTypes.KnowledgeBase
import org.controlsfx.dialog.WizardPane

import scala.concurrent.ExecutionContext.Implicits.global
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.beans.binding.BooleanBinding
import scalafx.beans.property.{BooleanProperty, DoubleProperty}
import scalafx.geometry.Insets
import scalafx.scene.control.Label
import scalafx.scene.layout.{BorderPane, HBox, StackPane}
import scalafx.scene.text.{Font, Text}

private[wizards] class InconsistencyMeasureResultPage(element: ObservableProjectElement, measure: InconsistencyMeasureModule)
  extends WizardPane with I18nSupport with AnalysisWorker with ErrorHandling with PleaseWait {

  private val result = new DoubleProperty()
  result set -1
  private val resultAvailable = BooleanProperty(value = false)
  private val elapsedLabel = new Label()

  result onChange {
    resultAvailable set true
  }

  val waitingProperty : BooleanBinding = resultAvailable.not and errorWhileCalculation.not

  private val resultLabel: Label = new Label {
    padding = Insets(10, 10, 10, 10)
    visible bind resultAvailable
    font = Font.font("sans-serif", FontWeight.BOLD, 16)
    text.bind(new SimpleStringProperty("analysis.inconsistencymeasure.label".i18n()+" ") concat Bindings.convert(result))
  }

  private val pane: BorderPane = new BorderPane {
    top = new HBox {
      padding = Insets(10, 10, 20, 10)
      children = new Text {
        text = "analysis.inconsistencymeasure.title".i18n(element.nameProperty.value, i18n_inconsistencyMeasure(measure.identify))
        font = new Font("sans-serif", 16)
      }
    }

    center = new StackPane {
      children = List(
        resultLabel,
        errorPane,
        pleaseWaitPane
      )
    }

    bottom = new HBox {
      padding = Insets(10, 10, 10, 10)
      children = List(elapsedLabel)
    }
  }

  val successHandler = (parseSuccesses: WorkerSuccess) => {
    val base: KnowledgeBase = parseSuccesses.results.map(_.formula)
    val startTime = System.nanoTime()
    measure bind base flatMap (_ measure base) onSuccess {
      case value: Double =>
        Platform.runLater {
          val elapsedTime = parseSuccesses.time.plus(System.nanoTime() - startTime, ChronoUnit.NANOS)
          elapsedLabel.setText("analysis.general.elapsedtime.label".i18n(elapsedTime.toMillis + ""))
          result.set(value)
          setGraphic(null)
          ()
        }
    }
  }
  setContent(pane)
  runWorker(element)

}
