package de.feu.shapley.app.types

import java.nio.file.Path

import scala.collection.JavaConversions._
import scala.language.implicitConversions
import scalafx.beans.property.ObjectProperty
import scalafx.collections.ObservableBuffer

/**
 * Observation proxy for [[Project]] that also contains a project storage location
 * @param delegate to proxy
 * @author Alexander Erben (a_erben@outlook.com)
 */
class ObservableProject(val delegate: Project) {

  /**
   * Contains the [[ProjectElement]]s of the proxied [[Project]]
   */
  val elementsProperty: ObservableBuffer[ObservableProjectElement] = ObservableBuffer()

  /**
    * Contains the location of the proxied [[Project]].
    * Initially, none is present.
    */
  val locationProperty: ObjectProperty[Option[Path]] = ObjectProperty[Option[Path]](delegate.location)

  private val observables = delegate.elements map (new ObservableProjectElement(_))
  elementsProperty addAll observables
  elementsProperty onChange ((buffer, _) => {
    val unwrapped = buffer.toList.map(_.delegate)
    delegate.elements = unwrapped
  })
  locationProperty onChange ((_, _, newProperty) => {
    delegate.location = newProperty
  })

  def hasUncommitedChanges:Boolean = {
    elementsProperty.map(element => element.hasUncommitedChanges).exists(identity)
  }

  def commitAll() = {
    elementsProperty.foreach(_.commit())
  }
  implicit def toProject(observable:ObservableProject):Project = observable.delegate
}
