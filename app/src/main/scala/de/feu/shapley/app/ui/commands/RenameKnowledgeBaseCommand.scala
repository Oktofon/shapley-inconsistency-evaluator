package de.feu.shapley.app.ui.commands

import de.feu.shapley.app.ui.stages.ProjectStage

/**
  * Issued when a knowledge base is to be renamed
  */
class RenameKnowledgeBaseCommand(name: String)(implicit val state:ProjectStage) extends Command{

  private val oldName = state.currentElement.value.nameProperty.value
  override def apply(): Unit = {
    state.currentElement.value.nameProperty.set(name)
  }

  override def revert(): Unit = {
    state.currentElement.value.nameProperty.set(oldName)
  }
}
