package de.feu.shapley.app.ui.stages

import java.io.File
import javafx.event.ActionEvent

import de.feu.shapley.app.io.ProjectStorageFunctions
import de.feu.shapley.app.types.Project
import de.feu.shapley.app.ui.Includes._
import de.feu.shapley.app.ui.components.{HelpTarget, I18nSupport}

import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.scene.layout.VBox
import scalafx.scene.text.Font
import scalafx.stage.FileChooser.ExtensionFilter
import scalafx.stage.{FileChooser, Stage}

/**
 * Offers options available on application startup
 */
class ProjectSelectStage extends PrimaryStage with I18nSupport with HelpTarget {

  /** For use in lambdas as outer reference */
  private val selectStage = this

  override val helpId = "start"

  /** When the last project window is closed, show this window again */
  ProjectStage.registerAllClosedEvent(() => this.show())

  title = "application.title".i18n()

  /**
   * Button used to create a new empty project
   */
  private val newProjectButton = new Button() {
    text = "projectselect.new".i18n()
    prefWidth = 200
    onAction = (e: ActionEvent) => {
      selectStage.hide()
      val stage: Stage = ProjectStage.newEmpty()
      stage.show()
    }
  }

  /**
   * Button used to load a pre-existing project file from disk
   */
  private val loadProjectButton = new Button() {
    text = "projectselect.load".i18n()
    prefWidth = 200
    onAction = (_: ActionEvent) => {
      val fileChooser = new FileChooser() {
        extensionFilters.add(new ExtensionFilter("project.extensionfilter".i18n(), "*.siv"))
        initialFileName = "*.siv"
      }
      val file: File = fileChooser.showOpenDialog(ProjectSelectStage.this)
      if (file != null) {
        selectStage.hide()
        val project: Project = ProjectStorageFunctions.loadProject(file)
        val stage: Stage = ProjectStage.newFromElements(project)
        stage.show()
      }
    }

  }

  /**
   * Button used to load an example project
   */
  private val exampleProjectButton = new Button() {
    text = "projectselect.loadexample".i18n()
    prefWidth = 200
    onAction = (_: ActionEvent) => {
      selectStage.hide()
      val stage: Stage = ProjectStage.newWithDefaultElements()
      stage.show()
    }
  }


  scene = new Scene {
    content = new VBox {
      padding = Insets(20, 20, 20, 20)
      spacing = 10
      children = List(
        new Label {
          text = "projectselect.title".i18n()
          font = Font("sans-serif", 16)
        }, newProjectButton, loadProjectButton, exampleProjectButton, helpButton()
      )
    }
  }
}
