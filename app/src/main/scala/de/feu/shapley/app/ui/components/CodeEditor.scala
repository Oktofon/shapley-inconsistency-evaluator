/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.app.ui.components

import java.util
import java.util.Collections
import java.util.Collections.emptyList
import java.util.regex.Pattern
import javafx.beans.value.{ChangeListener, ObservableValue}

import de.feu.shapley.app.types.ObservableProjectElement
import org.fxmisc.richtext.{CodeArea, LineNumberFactory, StyleSpans, StyleSpansBuilder}

/**
  * Provides means to edit the content of a knowledge base.
  * Has its own undo redo handler that overrides the handler of [[de.feu.shapley.app.ui.stages.ProjectStage]]
  * when the focus is on the editor
  */
class CodeEditor (pe: ObservableProjectElement) extends CodeArea {

  private val keywords = List("\\*", "\\+", "->", "!", "<->", "\\(", "\\)")
  private val keywordPattern: Pattern = Pattern.compile("(" + keywords.mkString("|") + ")")
  textProperty().addListener(new ChangeListener[String] {
    override def changed(observable: ObservableValue[_ <: String], oldValue: String, newValue: String) = {
      setStyleSpans(0, computeHighlighting(newValue))
    }
  })
  setParagraphGraphicFactory(LineNumberFactory.get(this))
  setPrefSize(650, 325)
  setMinSize(650, 325)
  replaceText(pe.knowledgeBaseProperty.value)

  pe.uncommitedChangesProperty.bind(textProperty())
  getUndoManager.forgetHistory()
  def revertChanges(): Unit = {
    val newValue: String = pe.knowledgeBaseProperty.value
    replaceText(newValue)
    setStyleSpans(0, computeHighlighting(newValue))
  }

  def computeHighlighting(text: String): StyleSpans[util.Collection[String]] = {
    val matcher = keywordPattern.matcher(text)
    var lastKwEnd = 0
    val spansBuilder: StyleSpansBuilder[util.Collection[String]] = new StyleSpansBuilder()
    while (matcher.find()) {
      spansBuilder.add(emptyList(), matcher.start() - lastKwEnd)
      spansBuilder.add(Collections.singleton("keyword"), matcher.end() - matcher.start())
      lastKwEnd = matcher.end()
    }
    spansBuilder.add(emptyList(), text.length() - lastKwEnd)
    spansBuilder.create()
  }
}