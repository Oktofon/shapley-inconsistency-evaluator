package de.feu.shapley.app.ui.commands

import de.feu.shapley.app.ui.stages.ProjectStage

/**
  * Issued when knowledge base is to be deleted
  */
class DeleteKnowledgeBaseCommand(implicit val state: ProjectStage) extends Command {
  val current = Option(state.currentElement.value)

  override def apply(): Unit = {
    synchronized {
      current foreach ((current) =>
        state.currentElements.remove(state.currentElements indexOf current))
    }
  }

  override def revert(): Unit = {
    current foreach (state.currentElements add _)
  }
}
