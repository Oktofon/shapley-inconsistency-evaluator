package de.feu.shapley.app.ui.wizards


import java.io.File
import java.text.DecimalFormat
import java.time.Duration

import com.github.tototoshi.csv.{CSVWriter, DefaultCSVFormat}
import com.norbitltd.spoiwo.model._
import com.norbitltd.spoiwo.natures.xlsx.Model2XlsxConversions._
import com.typesafe.scalalogging.Logger
import de.feu.shapley.app.types.ObservableProjectElement
import de.feu.shapley.app.ui.components.{I18nSupport, PleaseWait}
import de.feu.shapley.core.measurement.InconsistencyMeasureModule
import de.feu.shapley.core.shapley.ShapleyValueCalculator
import de.feu.shapley.core.types.PropositionalTypes._
import org.controlsfx.dialog.WizardPane
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.beans.binding.BooleanBinding
import scalafx.beans.property._
import scalafx.collections.ObservableBuffer
import scalafx.event.ActionEvent
import scalafx.geometry.Insets
import scalafx.scene.control.TableColumn._
import scalafx.scene.control._
import scalafx.scene.layout.{BorderPane, HBox, StackPane, VBox}
import scalafx.scene.text.{Font, Text}
import scalafx.stage.FileChooser
import scalafx.stage.FileChooser.ExtensionFilter

private[wizards] class ShapleyValueResultPage(element: ObservableProjectElement,
                                              measure: InconsistencyMeasureModule)
  extends WizardPane with I18nSupport with AnalysisWorker with ErrorHandling with PleaseWait {

  private val logger = Logger(LoggerFactory getLogger getClass)

  private val results = new ObservableBuffer[ShapleyValueResultRow]()
  private val resultsAvailable = BooleanProperty(value = false)
  private val elapsedLabel = new Label()

  private val fileChooser = new FileChooser() {
    extensionFilters.addAll(
      new ExtensionFilter("analysis.shapleyvalue.filechooser.csv.label".i18n(), "*.csv"),
      new ExtensionFilter("analysis.shapleyvalue.filechooser.excel.label".i18n(), "*.xlsx"))
  }

  private val exportButton = new Button("analysis.shapleyvalue.button.export".i18n()) {
    onAction = (a: ActionEvent) => {
      fileChooser.showSaveDialog(null) match {
        case null =>
        case csvFile if csvFile.getName.endsWith(".csv") => exportToCsv(csvFile)
        case xlsxFile if xlsxFile.getName.endsWith(".xlsx") => exportToXlsx(xlsxFile)
        case _ =>
      }
      ()
    }
    visible bind resultsAvailable
  }

  val waitingProperty: BooleanBinding = resultsAvailable.not and errorWhileCalculation.not

  results onChange ((buffer, _) => resultsAvailable set buffer.nonEmpty)

  private val resultTable: HBox = new HBox {
    padding = Insets(10, 0, 10, 0)
    visible bind resultsAvailable
    children = new TableView[ShapleyValueResultRow](results) {
      style = "-fx-focus-color: grey;"
      columns ++= List(new TableColumn[ShapleyValueResultRow, String] {
        sortable = false
        text = "analysis.shapleyvalue.table.column.formula".i18n()
        cellValueFactory = _.value.propositionalExpression
        prefWidth = 400
      },
        new TableColumn[ShapleyValueResultRow, String] {
          text = "analysis.shapleyvalue.table.column.value".i18n()
          cellValueFactory = _.value.result
          prefWidth = 180
        }
      )
    }
  }

  val successHandler: (WorkerSuccess => Unit) = (workerSuccess: WorkerSuccess) => {
    val startTime = System.nanoTime()
    ShapleyValueCalculator.shapleyOfAll(workerSuccess.results.map(_.formula), measure)
      .map(shapleyResults => {
        val duration = Duration.ofNanos(System.nanoTime() - startTime).plus(workerSuccess.time)
        Platform.runLater(elapsedLabel.setText("analysis.general.elapsedtime.label" i18n duration.toMillis.toString))
        shapleyResults.values
          .zip(workerSuccess.results.map(_.source))
          .foreach { case (shapleyValue, exprsString) => results.add(
            new ShapleyValueResultRow(exprsString, shapleyValue)) }
      }
      )
  }

  private val pane: BorderPane = new BorderPane {
    top = new HBox {
      padding = Insets(10, 0, 10, 0)
      children = new Text {
        text = "analysis.inconsistencymeasure.title".i18n(element.nameProperty.value,
          i18n_inconsistencyMeasure(measure.identify))
        font = new Font("sans-serif", 16)
      }
    }

    center = new StackPane {
      children = List(
        resultTable,
        errorPane,
        pleaseWaitPane
      )
    }

    bottom = new VBox {
      spacing = 5
      children = List(elapsedLabel, exportButton)
    }
  }
  setContent(pane)
  runWorker(element)
  Platform runLater {
    setGraphic(null)
    ()
  }

  private def exportToXlsx(file: File): Unit = {
    val sheet = Sheet(name = s"${element.nameProperty.value} ${i18n_inconsistencyMeasure(measure.identify)}")
      .addRow(Row().withCellValues("analysis.shapleyvalue.excel.column.formula".i18n(),
        "analysis.shapleyvalue.excel.column.value".i18n()))
      .addRows(results.map(result => Row().withCellValues(result.expression, result.value)))
    sheet.saveAsXlsx(file.toString)
  }

  private def exportToCsv(file: File): Unit = {
    implicit object MyFormat extends DefaultCSVFormat {
      override val delimiter: Char = {
        val delimiterConfiguration = "analysis.shapleyvalue.csv.delimiter".i18n()
        if (delimiterConfiguration.length == 1) {
          logger warn s"Delimiter misconfigured. Defaulting to semicolon. Was: $delimiterConfiguration"
          ';'
        } else {
          delimiterConfiguration charAt 0
        }
      }
    }
    val writer = CSVWriter.open(file)
    results.foreach(result => {
      writer.writeRow(Seq(result.expression, result.value))
    })
    writer.close()
  }

  /**
    * Value class for the result of a propositional expression shapley value calculation
    * For UI displaying use only
    *
    * @author Alexander Erben (a_erben@outlook.com)
    */
  class ShapleyValueResultRow(val expression: String, val value: ShapleyValue) {

    /** UI property for the propositional expression displayed */
    val propositionalExpression = new StringProperty(expression)

    /** UI property for the calculation result. */
    val result = new StringProperty(new DecimalFormat("#.#####").format(value))

  }

}
