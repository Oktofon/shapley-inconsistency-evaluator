package de.feu.shapley.app.ui.components

import java.util.function.Consumer
import javafx.event.ActionEvent
import javafx.scene.control.ButtonType

import com.typesafe.scalalogging.Logger
import de.feu.shapley.app.types.ObservableProjectElement
import de.feu.shapley.app.ui.Includes._
import de.feu.shapley.app.ui.stages.SyntaxCheckStage
import de.feu.shapley.app.ui.wizards.AnalysisWizard
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.language.implicitConversions
import scalafx.application.Platform
import scalafx.beans.property.{BooleanProperty, ObjectProperty}
import scalafx.geometry.Insets
import scalafx.scene.control._
import scalafx.scene.layout.{BorderPane, Priority, StackPane}

/**
  * Holds the [[CodeEditor]] and the toolbar above it
  */
class EditorPane(selectedElementProperty:ObjectProperty[ObservableProjectElement], elementsEmptyProperty:BooleanProperty) extends StackPane with I18nSupport {

  private val logger = Logger(LoggerFactory getLogger this.getClass)

  private val editors:mutable.HashMap[ObservableProjectElement, CodeEditor] = new mutable.HashMap()

  /** button used to save the changes in the visible code editor */
  private val saveProjectElementButton = new Button() {
    text = "editor.button.save".i18n()
    onAction = (e: ActionEvent) => {
      selectedElementProperty.value.commit()
    }
  }

  /** button used to revert the changes in the visible code editor */
  private val discardChangedButton = new Button() {
    text = "editor.button.discard".i18n()
    onAction = (e: ActionEvent) => {
      editors.get(selectedElementProperty.value) foreach (_.revertChanges())
      ()
    }
  }

  /** button used to open the syntax check panel */
  private val syntaxCheckButton = new Button() {
    text = "editor.button.syntaxcheck".i18n()
    onAction = (e: ActionEvent) => {
      new SyntaxCheckStage(selectedElementProperty.value).showAndWait()
    }
  }

  /** Bind the enabled state of the save and discard buttons in the toolbar */
  selectedElementProperty onChange ((_, _, newValue) => {
    if (newValue != null) {
      saveProjectElementButton.disable bind newValue.uncommitedChangesPresentProperty.not
      discardChangedButton.disable bind newValue.uncommitedChangesPresentProperty.not
      applyCodeEditor()
    }
  })

  /** button used to start evaluation of the currently visible knowledge base */
  private val evalKnowledgeBaseButton = new Button {
    text = "editor.button.analysis".i18n()
    onAction = (ev: ActionEvent) => {
      new AnalysisWizard(selectedElementProperty.value).showAndWait().ifPresent((b: ButtonType) =>
        if (b == ButtonType.FINISH) {
          logger.debug("Finished analysis")
        }
      )
    }
  }

  val mainContentPane = new BorderPane {
    hgrow = Priority.Always
    visible bind elementsEmptyProperty.not
    top = new ToolBar {
      content = List(
        saveProjectElementButton,
        discardChangedButton,
        syntaxCheckButton,
        evalKnowledgeBaseButton)
    }
  }


  def applyCodeEditor() = {
    if (elementsEmptyProperty.not.getValue) {
      val newEditor: CodeEditor = editors.getOrElse(selectedElementProperty.value, new CodeEditor(selectedElementProperty.value))
      newEditor.visibleProperty bind elementsEmptyProperty.not
      editors.put(selectedElementProperty.value, newEditor)
      mainContentPane.setCenter(newEditor)
      Platform.runLater(mainContentPane.prefWidth bind EditorPane.this.getScene.widthProperty())
    }
  }

  applyCodeEditor()

  children = List(
    mainContentPane,
    new StackPane {
      padding = Insets(10, 10, 10, 10)
      visible bind elementsEmptyProperty
      children = new Label {
        text = "editor.knowledgebase.pleaseopen".i18n()
      }
    }
  )

  /**
    * Bridge the Java 8 Consumer API to a Scala function
    */
  implicit def toConsumer[A](function: A => Unit): Consumer[A] = new Consumer[A]() {
    override def accept(arg: A): Unit = function.apply(arg)
  }
}