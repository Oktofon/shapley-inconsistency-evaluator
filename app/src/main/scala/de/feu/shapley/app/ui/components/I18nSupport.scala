/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.app.ui.components

import java.text.MessageFormat
import java.util.Locale
import java.util.ResourceBundle.getBundle

import de.feu.shapley.core.Util.UTF8Control
import de.feu.shapley.core.provider.PluginTranslationProvider

/**
  * Adds internationalization functionality
  *
  * @author Alexander Erben
  */
trait I18nSupport {

  /**
    * Application i18n bundle
    */
  private val applicationBundle = getBundle("shapleyapp", currentLocale, new UTF8Control)

  /**
    * Inconsistency i18n bundle mapping implementations
    * of the [de.feu.shapley.core.operations.measurement.InconsistencyMeasures]
    */
  private val inconsistencyBundle = getBundle("inconsistencymeasures", currentLocale, new UTF8Control)

  private def currentLocale: Locale = Locale.getDefault()

  /**
    * Retrieve an internationalized string for the current locale
    *
    * @param key the key to retrieve a value for
    * @return the internationalized string
    */
  def translate(key: String, parameters: String*): String = {
    if (!applicationBundle.keySet().contains(key))
      key
    else
      MessageFormat.format(applicationBundle.getString(key), parameters.toArray: _*)
  }

  /**
    * Enhance a string to use it as i18n key
    *
    * @param value to use as key
    */
  implicit class I18nString(value: String) {
    def i18n(parameters: String*): String = {
      translate(value, parameters: _*)
    }

    def i18n(): String = {
      translate(value)
    }
  }

  /**
    * Retrieve an internationalized string for an inconsistency measure
    *
    * @param key the key of the inconsitency measure
    * @return the internationalized string
    */
  def i18n_inconsistencyMeasure(key: String) = {
    if (inconsistencyBundle.containsKey(key))
      inconsistencyBundle.getString(key)
    else
      PluginTranslationProvider.translate(key)
  }

}
