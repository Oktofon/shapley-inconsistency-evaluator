package de.feu.shapley.app.io

import java.io.{File, FileInputStream, FileOutputStream, OutputStreamWriter}

import de.feu.shapley.app.types.{Project, ProjectElement}
import de.feu.shapley.app.ui.components.I18nSupport
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor

import scala.beans.BeanProperty
import scala.collection.JavaConverters._

/**
  * Handles storage and retrieval of project elements and provides a set of default elements for convenience
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
object ProjectStorageFunctions extends I18nSupport {

  /** Empty null implementation of [[Project]] */
  def nilProject: Project = Project.create(List())

  /** Empty null implementation of [[ProjectElement ]] */
  def nilProjectElement: ProjectElement = ProjectElement.create("test", "test")

  /** Example default [[Project]]s */
  def defaultProject: Project = {
    val first = "a * b,a * (c + d),a * !d,a * !c * e,!a * !b,a * (!c -> !e),a * !c * f".replace(',', '\n')
    val second = "a,b,b * c,!b * d".replace(',', '\n')
    val third = "a,!a,!a * c,a + d,!d,b * b, e".replace(',', '\n')
    val elements = List(ProjectElement.create(translate("project.examplek13.title"), first),
      ProjectElement.create(translate("project.examplehk06.title"), second),
      ProjectElement.create(translate("project.smallkb.title"), third))
    Project.create(elements)
  }

  /**
    * Load a [[Project]] from file
    *
    * @param from targeting the file to load. Must exist.
    * @return loaded [[Project]]
    */
  def loadProject(from: File): Project = {
    require(from.exists())
    val project: SerializeProject = new Yaml(new Constructor(classOf[SerializeProject]))
      .load(new FileInputStream(from)).asInstanceOf[SerializeProject]
    new Project(project.id, project.elements.asScala.toList, Some(from.toPath))
  }

  /**
    * Store a [[Project]] to file
    *
    * @param that project to store
    */
  def storeProject(that: Project) = {
    that match {
      case Project(_, _, None) =>
        throw new IllegalArgumentException("Project has no location yet!")
      case Project(id,elements, Some(location)) =>
        new Yaml(new Constructor(classOf[SerializeProject]))
          .dump(new SerializeProject(id, elements.asJava),
            new OutputStreamWriter(
              new FileOutputStream(location.toFile)))
    }
  }

  /**
    * Encapsulates the knowledgebases a user manages, an ID and a name
    *
    * @author Alexander Erben (a_erben@outlook.com)
    */
  class SerializeProject(@BeanProperty var id: String = null,
                         @BeanProperty var elements: java.util.List[ProjectElement] = null) {

    def this() = {
      this(null, null)
    }

    override def toString: String = id
  }

}


