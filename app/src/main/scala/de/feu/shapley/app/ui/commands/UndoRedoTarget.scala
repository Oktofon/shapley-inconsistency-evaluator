package de.feu.shapley.app.ui.commands

import de.feu.shapley.app.ui.RestrictedStack

import scalafx.beans.property.BooleanProperty

/**
  * Implementors are eligible for undo and redo via the edit menu
  */
trait UndoRedoTarget {

  /**
    * The last 10 undoable operations.
    */
  val undoStack:RestrictedStack = new RestrictedStack(10)

  /**
    * The last 10 redoable undone operations.
    */
  val redoStack:RestrictedStack  = new RestrictedStack(10)
  val undoAvailable: BooleanProperty = undoStack.nonEmptyProperty
  val redoAvailable: BooleanProperty = redoStack.nonEmptyProperty

  /**
    * Inform the handler of an operation that has been performed by the user which can be undone.
    * this will invalidate the current stack of redo-able operations.
    *
    * @param command to push to the undo stack.
    */
  def execute(command: Command) {
    synchronized {
      command.apply()
      redoStack.clear()
      undoStack.push(command)
    }
  }

  /**
    * Undo the last operation that can be undone and push it to the redo stack.
    * Does nothing if no operation is present that can be undone.
    */
  def undo() = {
    synchronized {
      undoStack.pop() foreach (command => {
        command.revert()
        redoStack.push(command)
      })
    }
  }

  /**
    * Redo the last undone operation and push it to the undo stack.
    * Does nothing if no operation is present that can be undone.
    */
  def redo() = {
    synchronized{
      redoStack.pop() foreach (command => {
        command.apply()
        undoStack push command
      })
    }
  }


  def resetHistory() = {
    undoStack.clear()
    redoStack.clear()
  }
}
