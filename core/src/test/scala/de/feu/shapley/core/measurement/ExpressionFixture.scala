/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.core.measurement

import de.feu.shapley.core.parsers.CNF
import de.feu.shapley.core.parsers.ParserExpressions._
import de.feu.shapley.core.types.PropositionalTypes._
import org.scalatest.prop.Tables.Table
trait ExpressionFixture {

  def toE(p:ParserExpression):Formula = {
    CNF(p)._1
  }

  val consistentExpressions =
    Table(
      "expression",
      toE('ab * 'ab),
      toE('a * 'a),
      toE('a * 'b),
      toE(('a -> 'b) * ('a * 'b)),
      toE(T),
      toE(T * 'whatever),
      toE('a * ('c + 'd)),
      toE('a * Not('d)),
      toE('a * Not('c) * 'e),
      toE(Not('a) * Not('b)),
      toE('a * (Not('c) -> Not('e))),
      toE('a * Not('c) * 'f      )
    )

  def K(expr: Formula*): KnowledgeBase = expr.toList

  val inconsistentExpressions =
    Table(
      "expression",
      toE(F),
      toE(F * T),
      toE(F * 'test),
      toE('a * Not('a)),
      toE('a * Not('a) * 'c),
      toE(('a -> 'b) * 'a * Not('b)),
      toE('a * Not('a) * Not('a) * 'c * 'a + 'd * Not('d) * 'b * 'b * 'e)
    )
}
