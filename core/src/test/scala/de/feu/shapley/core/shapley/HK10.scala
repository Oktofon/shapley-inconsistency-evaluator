package de.feu.shapley.core.shapley

import de.feu.shapley.core.measurement.{DrasticInconsistencyMeasureModule, InconsistencyMeasureModule, MUSDrasticInconsistencyMeasureModule, MinimumInconsistentSubsetMeasureModule}
import de.feu.shapley.core.parsers.FormulaParser.parseAll
import de.feu.shapley.core.parsers.ParseSuccess
import de.feu.shapley.core.shapley.ShapleyValueCalculator._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{FlatSpec, Matchers}

import scala.io.Source

/**
  * Tests from "On the Measure of Conflicts"
  * by Anthony Hunter and Sébastien Konieczny
  */
class HK10 extends FlatSpec with Matchers with ScalaFutures {
  implicit val defaultPatience =
    PatienceConfig(timeout = Span(100, Seconds), interval = Span(5, Millis))


  def checkDrastic(exampleNumber:Int, values:List[Double]) = {
    checkInOrder(DrasticInconsistencyMeasureModule, exampleNumber, values)
    checkInOrder(MUSDrasticInconsistencyMeasureModule, exampleNumber, values)
  }

  def checkMi(exampleNumber:Int, values:List[Double])  = {
    checkInOrder(MinimumInconsistentSubsetMeasureModule, exampleNumber, values)
  }

  def checkInOrder(im: InconsistencyMeasureModule, exampleNr: Int, values: List[Double]) = {
    val formulasAsString =
      Source
      .fromInputStream(getClass.getClassLoader.getResourceAsStream(s"hk10/example$exampleNr.txt"))
      .getLines()
    whenReady(parseAll(formulasAsString.toList)) { exprs =>
      val parsed = exprs.map(pr => pr.asInstanceOf[ParseSuccess].formula)
      whenReady(shapleyOfAll(parsed, im)) { a =>
        for (idx <- values.indices) {
          a(parsed(idx)) shouldBe (values(idx) +- 1e-5)
        }
      }
    }
  }

  "HK10 Example 9" must "match the proposed results" in {
    checkDrastic(9, List(1d / 2d, 1d / 2d, 0))
  }

  "HK10 Example 10" must "match the proposed results" in {
    checkDrastic(10, List(0, 1d / 6d, 1d / 6d, 4d / 6d))
  }

  "HK10 Example 11" must "match the proposed results" in {
    checkDrastic(11, List(4d / 6d, 1d / 6d, 1d / 6d, 0))
  }

  "HK10 Example 12" must "match the proposed results" in {
    checkMi(12, List(1d / 2d, 1d / 2d, 0))
  }

  "HK10 Example 13" must "match the proposed results" in {
    checkMi(13, List(0, 1d / 2d, 1d / 2d, 1))
  }

  "HK10 Example 14" must "match the proposed results" in {
    checkMi(14, List(1, 1d / 2d, 1d / 2d, 0))
  }

  "HK10 Example 15" must "match the proposed results" in {
    checkMi(15, List(1, 1, 1d / 2d, 1d / 2d))
  }

  "HK10 Example 16" must "match the proposed results" in {
    checkMi(16, List(3d / 2d, 1d / 2d, 1d / 2d, 1d / 2d))
  }

  "HK10 Example 17" must "match the proposed results" in {
    checkMi(17, List(5d / 6d, 1d / 2d, 1d / 3d, 1d / 3d))
  }

}
