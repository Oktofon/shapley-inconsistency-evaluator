/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.core.measurement

import de.feu.shapley.core.types.PropositionalTypes._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.prop.PropertyChecks
import scala.concurrent.ExecutionContext.Implicits.global

class DrasticInconsistencyMeasureTest extends org.scalatest.PropSpec with PropertyChecks with ExpressionFixture with ScalaFutures {

  property("Propositional expression") {

    forAll(consistentExpressions) {
      expression =>
        whenReady(DrasticInconsistencyMeasureModule bind expression flatMap (_.measure(expression))) { result =>
          assertResult(0)(result)
        }
    }

    forAll(inconsistentExpressions) {
      expression =>
        whenReady(DrasticInconsistencyMeasureModule bind expression flatMap (_.measure(expression))) { result =>
          assertResult(1)(result)
        }
    }
  }
}
