package de.feu.shapley.core.parsers

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Minutes, Millis, Seconds, Span}
import org.scalatest.{FlatSpec, Matchers}

/**
  * Parser tests
  *
  * @author Alexander Erben
  */
class ExpressionParserTest extends FlatSpec with Matchers with ScalaFutures {
  implicit val defaultPatience =
    PatienceConfig(timeout = Span(2, Minutes), interval = Span(5, Millis))

  private def matchResult(result: ParseResult, comparee: List[List[Int]]): Unit = {
    result match {
      case ParseSuccess(expr, _, ctx) =>
        expr shouldBe comparee
      case ParseFailure(e, _, _) =>
        throw e
    }
  }

  "The parser" must "parse a single conjunction" in {
    whenReady(FormulaParser.parse("a * b")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(vars("a")), List(vars("b"))))
    }
  }


  "The parser" must "parse a conjunction with a negation" in {
    whenReady(FormulaParser.parse("a * !b")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(vars("a")), List(-vars("b"))))
    }
  }
  "The parser" must "parse a conjunction with two negations" in {
    whenReady(FormulaParser.parse("!a * !b")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(-vars("a")), List(-vars("b"))))
    }
  }

  "The parser" must "parse a combined conjunction and disjunction, honouring the operator precedence" in {
    whenReady(FormulaParser.parse("(a * b) + c")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(vars("a"), vars("c")), List(vars("b"), vars("c"))))
    }
  }

  "The parser" must "parse a chained conjunction with negation" in {
    whenReady(FormulaParser.parse("(a * !b) * c")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(vars("a")), List(-vars("b")), List(vars("c"))))
    }
  }

  "The parser" must "parse a conjunction with an implication" in {
    whenReady(FormulaParser.parse("(a * b) -> c")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(-vars("a"), -vars("b"), vars("c"))))
    }
  }

  "The parser" must "parse \"a * (!b -> !c)\"" in {
    whenReady(FormulaParser.parse("a * (!b -> !c)")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(vars("a")), List(vars("b"), -vars("c"))))
    }
  }

  "The parser" must "handle nested parentheses" in {
    whenReady(FormulaParser.parse("((a * b) * (b * c))")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(vars("a")),List(vars("b")),List(vars("b")), List(vars("c"))))
    }
  }

  "The parser" must "handle nested parentheses with negation" in {
    whenReady(FormulaParser.parse("(!(a * b) * (b * c))")) { result =>
      val vars = result.ctx
      matchResult(result, List(List(-vars("a"), -vars("b")),List(vars("b")), List(vars("c"))))
    }
  }

  "The parser" must "handle A -> (B * (!C <-> !(!E + F)))" in {
    whenReady(FormulaParser.parse("a -> (b * (!c <-> !(!e + f)))")) { result =>
      val vars: ParseContext = result.ctx
      matchResult(result, List(List(-vars("a"), vars("b")), List(-vars("a"), vars("c"), vars("e")), List(-vars("a"), vars("c"), -vars("f")),List(-vars("a"), -vars("c"), -vars("e"), vars("f"))))
    }
  }

  "The parser" must "handle !!a" in {
    whenReady(FormulaParser.parse("!!a")) { result =>
      val vars: ParseContext = result.ctx
      matchResult(result, List(List(vars("a"))))
    }
  }

  "The parser" must "handle !!!a" in {
    whenReady(FormulaParser.parse("!!!a")) { result =>
      val vars: ParseContext = result.ctx
      matchResult(result, List(List(-vars("a"))))
    }
  }

  "The parser" must "handle !!!!a" in {
    whenReady(FormulaParser.parse("!!!!a")) { result =>
      val vars: ParseContext = result.ctx
      matchResult(result, List(List(vars("a"))))
    }
  }
}