package de.feu.shapley.core.provider

import java.io.File
import java.nio.file.Files

import com.typesafe.scalalogging.Logger
import de.feu.shapley.core.ProviderSupport
import de.feu.shapley.core.measurement.{DrasticInconsistencyMeasureModule, InconsistencyMeasureModule, MUSDrasticInconsistencyMeasureModule, MinimumInconsistentSubsetMeasureModule}
import org.clapper.classutil.ClassFinder
import org.slf4j.LoggerFactory

import scala.language.postfixOps

/**
  * Provides access to user implementations of [[InconsistencyMeasureModule]] which reside
  * in the the .siv-directory in user home as well as the core module.
  */
object PluginAndCoreProvider extends InconsistencyMeasureProvider {

  private val logger = Logger(LoggerFactory getLogger this.getClass)

  // Load the definitions in the api module
  private val apiClasses = ProviderSupport.apiClasses

  private val coreImplementations = Seq(DrasticInconsistencyMeasureModule,
    MinimumInconsistentSubsetMeasureModule, MUSDrasticInconsistencyMeasureModule)

  override def findAll(): Set[InconsistencyMeasureModule] = {
    try {
      // Ensure that the user plugin directory exists
      Files createDirectories pluginPath
      val plugins = (pluginPath.toFile.listFiles() filter (_.isFile) flatMap toMeasure).toSet
      plugins ++ coreImplementations
    } catch {
      case e: Exception => logger.warn("Could not load plugins from file system", e)
        Set()
    }
  }

  /**
    * Loads all class definitions from an input file and tries to instantiates all subclasses of
    * [[InconsistencyMeasureModule]] found in the file
    */
  private def toMeasure(file: File): Set[InconsistencyMeasureModule] = {
    try {
      val classLoader = new java.net.URLClassLoader(
        Array(file.toURI.toURL),
        this.getClass.getClassLoader)
      val foundClasses = ClassFinder(Seq(file)).getClasses()
      val allClasses = foundClasses ++ apiClasses
      val plugins =
        ClassFinder.concreteSubclasses(classOf[InconsistencyMeasureModule].getCanonicalName, allClasses.toIterator)
      plugins
        .map(info => classLoader.loadClass(info.toString()))
        .map(_.newInstance.asInstanceOf[InconsistencyMeasureModule])
        .toSet
    } catch {
      case e:Exception => logger.warn("Error while reading plugin file "+file.getName, e)
        Set()
    }

  }
}
