package de.feu.shapley.core

import de.feu.shapley.core.types.PropositionalTypes._

import scala.util.Try

/**
  * Utility types for the parser of this application
  * @author Alexander Erben
  */
package object parsers {

  /** The [[ParseContext]] that contains no mapping at all */
  def emptyContext = Map[String, Int]()

  /**
    * A mapping from variable names as [[String]]s to [[Int]] values for use in a DIMACS-based CNF file format.
    * @author Alexander Erben
    */
  type ParseContext = Map[String, Int]

  /**
    * Result of the attempt to parse an input [[String]] to a [[Formula]]
    * @author Alexander Erben
    */
  sealed abstract class ParseResult {
    val ctx: ParseContext
    val source: String
  }

  /**
    * If the parsing succeeds, the result yields the parsed [[Formula]], the given [[String]] for convenience
    * and the new [[ParseContext]] that contains the given mapping plus and new variables discovered in the parsed
    * [[Formula]] mapped to an [[Int]] value
    * @param formula the parsed [[Formula]]
    * @param source the input [[String]]
    * @param ctx the new [[ParseContext]]
    */
  case class ParseSuccess(formula: Formula, source: String, ctx: ParseContext) extends ParseResult

  /**
    * If the parsing fails, the result contains the error that occured, the given [[String]] for convenience
    * and the unchanged [[ParseContext]]
    * @param reason the error that occured
    * @param source the input [[String]]
    * @param ctx the unchanged [[ParseContext]]
    */
  case class ParseFailure(reason: Throwable, source: String, ctx: ParseContext) extends ParseResult

}
