package de.feu.shapley.core.shapley

import de.feu.shapley.core.Util._
import de.feu.shapley.core.measurement.{BoundInconsistencyMeasure, InconsistencyMeasureModule}
import de.feu.shapley.core.types.PropositionalTypes._

import scala.annotation.tailrec
import scala.async.Async._
import scala.collection.immutable.ListMap
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Retrieves the [[ShapleyValue]] of a [[KnowledgeBase]] with a given
  * propositional [[Formula]] which is element of the base and an [[InconsistencyMeasureModule]]
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
object ShapleyValueCalculator {

  /**
    * Calculate the faculty of i
    * Tail recursive and thus not stack consuming.
    *
    * @return factorial of i
    */
  def faculty(i:Int) : Double = {
    require(i >= 0)
    @tailrec def factorialInner(n: Int, result: Int): Int = if (n == 0) result else factorialInner(n - 1, n * result)
    factorialInner(i, 1)
  }

  /**
    * Given a [[KnowledgeBase]] K, a propositional [[Formula]] α element of K and an [[BoundInconsistencyMeasure]] retrieveable
    * from a binder, calculate the [[ShapleyValue]].
    *
    * @param K the [[KnowledgeBase]]
    * @param α the [[Formula]] to calculate the [[ShapleyValue]] for. Must be an element of the [[KnowledgeBase]].
    * @param imModule the [[InconsistencyMeasureModule]] used to load an actual measure
    * @return the shapley value in the range of 0 to 1
    */
  def shapleyOf(K: KnowledgeBase, α: Formula, imModule: InconsistencyMeasureModule): Future[ShapleyValue] = {

    assert(K exists (_ equals α), s"Expression $α is not a part of ${K.toString()}")

    val measure = imModule.bind(K)

    def I(e: KnowledgeBase): Future[Double] = if (e.isEmpty) Future successful 0.0 else measure flatMap (_.measure(e))

    val n = K.size
    async {
      val subsets: Iterator[KnowledgeBase] = nonEmptySubsetsOf(K) filter (_ contains α)
      var sum = 0d
      while (subsets.hasNext) {
        val C = subsets.next
        val c = C.size
        val IC: Double = await {I(C)}
        if (IC != 0d) {
          val coalitionResult = faculty(c - 1) * faculty(n - c) / faculty(n) * (IC - await {I(C.remove(α))})
          sum = sum + coalitionResult
        }
      }
      sum
    }
  }

  /**
    * Calculate the [[ShapleyValue]] for all [[Formula]] of a [[KnowledgeBase]] asynchronously.
    *
    * @param K the [[KnowledgeBase]]
    * @param measure the [[BoundInconsistencyMeasure]]
    * @return a set of propositional [[Formula]] mapped to their [[ShapleyValue]] as [[Future]] instance
    */
  def shapleyOfAll(K: KnowledgeBase, measure: InconsistencyMeasureModule): Future[ShapleyCalculationResult] = {

    def toShapleyValueWithExpression(α: Formula) = shapleyOf(K, α, measure) map ((α, _))

    def toCalculationResult(lst: List[(Formula, ShapleyValue)]) =
      (lst foldLeft ListMap[Formula, ShapleyValue]())((map, item) => map.updated(item._1, item._2))

    val eventualTuples: List[Future[(Formula, ShapleyValue)]] = K map toShapleyValueWithExpression
    Future.sequence(eventualTuples) map toCalculationResult
  }

}
