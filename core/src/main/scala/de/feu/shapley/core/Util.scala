/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.core

import java.io.{InputStream, InputStreamReader}
import java.util.ResourceBundle.Control
import java.util.{Locale, PropertyResourceBundle, ResourceBundle}

import de.feu.shapley.core.types.PropositionalTypes.KnowledgeBase
import org.sat4j.core.VecInt
import org.sat4j.specs.IVecInt

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.language.implicitConversions

/**
  * Common application wide utility functions and types
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
object Util {

  /**
    * Generate all non-empty subsets of a [[de.feu.shapley.core.types.PropositionalTypes.KnowledgeBase]].
    *
    * @param K to generate from. Must not be empty.
    * @return all non-empty subsets
    */
  def nonEmptySubsetsOf(K: KnowledgeBase): Iterator[KnowledgeBase] = {
    assert(K.nonEmpty)
    Iterator.tabulate(K.size)(index => {
      K.combinations(index + 1)
    }).flatten
  }

  implicit def vecIntToList(vecInt: IVecInt): List[Int] = {
    val it = vecInt.iterator()
    val lb: ListBuffer[Int] = mutable.ListBuffer[Int]()
    while (it.hasNext) {
      lb += it.next()
    }
    lb.toList
  }

  implicit def listToVecInt(list: List[Int]): IVecInt = {
    val vecInt = new VecInt
    list.foreach(item => vecInt.push(item))
    vecInt
  }


  /**
    * Used to be able to load the property resource bundles with UTF-8 encoding
    */
  class UTF8Control extends Control {

    def streamToBundle(stream: InputStream) = {
      try {
        new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"))
      } finally {
        stream.close()
      }
    }

    override def newBundle(baseName: String, locale: Locale, format: String, loader: ClassLoader,
                           reload: Boolean): ResourceBundle = {
      val bundleName = toBundleName(baseName, locale)
      val resourceName = toResourceName(bundleName, "properties")
      val test: Option[PropertyResourceBundle] =
        if (reload) {
          Option(loader getResource resourceName)
            .flatMap(url => Option(url.openConnection()))
            .map(connection => {
              connection.setUseCaches(false)
              streamToBundle(connection.getInputStream)
            })
        } else {
          Option(loader getResourceAsStream resourceName) map streamToBundle
        }
      test.getOrElse(throw new IllegalStateException("Resource bundle not found"))
    }
  }

}
