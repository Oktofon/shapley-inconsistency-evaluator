/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Alexander Erben
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.feu.shapley.core.parsers

import scala.language.implicitConversions

/**
  * Propositional expressions that can be used in the parser
  * @author Alexander Erben (a_erben@outlook.com)
  */
object ParserExpressions {

  /**
    * Algebraic common type for all expressions of propositional logic supported by the parser grammer
    */
  sealed abstract class ParserExpression {

    def + = Disjunction(this, _: ParserExpression)

    def * = Conjunction(this, _: ParserExpression)

    def -> = Implication(this, _: ParserExpression)

    def <-> = Coimplication(this, _: ParserExpression)

    val components : List[ParserExpression]
  }

  /**
    * Shared behaviour of atomics that do not carry an operator
    */
  trait Atomic {
    val value: String
    val components = List()
    override def toString: String = value
  }

  /**
    * Shared behaviour of unary expressions
    */
  trait Unary {
    val operator: String
    val component: ParserExpression
    val components = List(component)
    override def toString: String = s"$operator($component)"
  }

  /**
    * Shared behaviour of binary expressions
    */
  trait Binary {
    val operator: String
    val left: ParserExpression
    val right: ParserExpression
    val components = List(left, right)
    override def toString: String = s"($left$operator$right)"
  }

  /**
    * Shared behaviour of associative expressions
    */
  trait Associative {
    val left: ParserExpression
    val right: ParserExpression

    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case that: Associative =>
          (left == that.left && right == that.right) || (left == that.right && right == that.left)
        case _ => false
      }
    }
  }

  /**
    * Enumeration of operator symbols for the ParserExpression types
    */
  object Operators {
    val elementaryNegation = "0"
    val elementaryTautology = "1"
    val conj = "*"
    val disj = "+"
    val impl = "->"
    val equi = "<->"
    val xor = "^"
    val not = "!"
  }

  /** The atom type derived from a scala symbol */
  case class Atom(value: String = "") extends ParserExpression with Atomic

  /** The elementary negation */
  case class Contradiction() extends ParserExpression with Atomic {
    val value = Operators.elementaryNegation
  }

  /** The elementary tautology */
  case class Tautology() extends ParserExpression with Atomic {
    val value = Operators.elementaryTautology
  }

  /** The implication operation */
  case class Implication(left: ParserExpression, right: ParserExpression) extends ParserExpression with Binary {
    val operator = Operators.impl
  }

  /** The equivalence operation */
  case class Coimplication(left: ParserExpression, right: ParserExpression) extends ParserExpression with Binary with Associative {
    val operator: String = Operators.equi
  }

  /** The conjunction operation */
  case class Conjunction(left: ParserExpression, right: ParserExpression) extends ParserExpression with Binary with Associative {
    val operator = Operators.conj
  }

  /** The disjoin operation */
  case class Disjunction(left: ParserExpression, right: ParserExpression) extends ParserExpression with Binary with Associative {
    val operator = Operators.disj
  }

  /** The negation operation */
  case class Not(component: ParserExpression) extends ParserExpression with Unary {
    val operator: String = Operators.not
  }

  /**
    * Alias for the tautology type
    *
    * @return tautology
    */
  def T = Tautology()

  /**
    * Alias for the negation type
    *
    * @return negation
    */
  def F = Contradiction()

  /**
    * Alias for the negation
    *
    * @return negation
    */
  def f = Not(_)

  /**
    * A symbol has the semantics of a propositional logic atom and hence may be treated as such.
    *
    * @param sym a symbol
    * @return the atom representation of the symbol
    */
  implicit def symbolToAtom(sym: Symbol): ParserExpression = Atom(sym.toString().substring(1))

  /**
    * Helper explicit to convert a propositional expression to a String representation
    *
    * @param E the expression to convert
    * @return a String representation
    */
  implicit def expressionAsString(E: ParserExpression): String = E.toString
}
