package de.feu.shapley.core.measurement

import de.feu.shapley.core.Util._
import de.feu.shapley.core.types.PropositionalTypes._
import org.sat4j.minisat.SolverFactory
import org.sat4j.specs.IGroupSolver
import org.sat4j.tools.AllMUSes

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * The module realising the miminum inconsistent subset measure.
  *
  * The MI Measure counts all minimum inconsistent subsets which are subsets of a set of expressions E
  * where removing any element of the set yields a consistent subset.
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
object MinimumInconsistentSubsetMeasureModule extends InconsistencyMeasureModule {

  override def bind(K: KnowledgeBase): Future[BoundInconsistencyMeasure] = Future {
    new Sat4JMinimumInconsistentSubsetMeasure(K)
  }

  override def identify = "mi"

  private[measurement] class Sat4JMinimumInconsistentSubsetMeasure(val domain: KnowledgeBase) extends BoundInconsistencyMeasure {

    val allmuses = new AllMUSes(true, SolverFactory.instance())
    val solver = allmuses.getSolverInstance.asInstanceOf[IGroupSolver]

    val indexMap: Map[Int, Formula] = (domain.indices.map(_ + 1) zip domain).toMap
    solver.newVar(domain.nVars)

    for ((index, formula) <- indexMap; clause <- formula) solver.addClause(clause, index)

    val musFuture = Future {
      val indexesOfMusses: List[List[Int]] = allmuses.computeAllMUSes().toList.map(vecIntToList)
      indexesOfMusses map (mus => mus map (indexMap(_)))
    }

    override def measure(K: KnowledgeBase) = musFuture map (mi => mi count (_ forall K.contains))

  }

}
