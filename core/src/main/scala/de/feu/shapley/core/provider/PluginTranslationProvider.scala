package de.feu.shapley.core.provider

import java.net.URLClassLoader
import java.nio.file.{Files, Path}
import java.util.{Locale, ResourceBundle}

import com.typesafe.scalalogging.Logger
import de.feu.shapley.core.Util.UTF8Control
import org.slf4j.LoggerFactory

/**
  * Provides translations for external [[de.feu.shapley.core.measurement.InconsistencyMeasureModule]]
  * implementations
  */
object PluginTranslationProvider {

  private val logger = Logger(LoggerFactory getLogger getClass)

  private def createBundleIfMissing(resourcesDirectory:Path) = {
    Files.createDirectories(resourcesDirectory)
    val bundleFile = resourcesDirectory.resolve("plugin.properties")
    if (!Files.exists(bundleFile)){
      Files.createFile(bundleFile)
    }
  }

  /**
    * Translate the given measure identification key with the resource bundle
    * given in the plugin directory
    * @param key to translate
    * @return translated key or the key itself if no translation is found
    */
  def translate(key: String): String = {
    try {
      def currentLocale: Locale = Locale.getDefault()
      val resourcesDirectory = pluginPath.resolve("resources")
      createBundleIfMissing(resourcesDirectory)
      val cl = new URLClassLoader(Array(resourcesDirectory.toFile.toURI.toURL))
      val translationBundle = ResourceBundle.getBundle("plugin", currentLocale,
        cl, new UTF8Control)
      if (translationBundle.containsKey(key)) {
        translationBundle.getString(key)
      } else {
        key
      }
    } catch {
      case e:Exception =>
        logger warn ("Could not load plugin resource bundle as it does not exist!", e)
        key
    }
  }
}
