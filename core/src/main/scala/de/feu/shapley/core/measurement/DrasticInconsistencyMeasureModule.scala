package de.feu.shapley.core.measurement

import de.feu.shapley.core.reasoning.SatisfiabilityTest._
import de.feu.shapley.core.types.PropositionalTypes._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * The module realising the drastic inconsistency measure
  * The drastic inconsistency measure has two possible return values:
  * 1 for an inconsistent expression set or expression and
  * 0 for a consistent expression set or expression
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
object DrasticInconsistencyMeasureModule extends InconsistencyMeasureModule {

  override def bind(K: KnowledgeBase): Future[BoundInconsistencyMeasure] =
    Future.successful(new DrasticInconsistencyMeasure(K))

  override def identify = "drastic"

  private class DrasticInconsistencyMeasure(val domain: KnowledgeBase) extends BoundInconsistencyMeasure {
    override def measure(K: KnowledgeBase) = Future {
      if (isSatisfiable(K)) 0d else 1d
    }
  }
}



