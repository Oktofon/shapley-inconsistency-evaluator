package de.feu.shapley.core.reasoning

import de.feu.shapley.core.types.PropositionalTypes._
import org.sat4j.core.{Vec, VecInt}
import org.sat4j.minisat.SolverFactory
import org.sat4j.specs.{ContradictionException, IVecInt}

/**
  * A consistency test that uses SAT4J as solving engine.
  * @author Alexander Erben
  */
object SatisfiabilityTest {

  /**
    * Checks if a given [[Formula]] is satisfiable. Satisfiable means that at least one distribution of
    * truth values over the variables in the given [[Formula]] makes it true.
    * @param expr to check
    * @return true if satisfiable, false if not
    */
  def isSatisfiable(expr: Formula): Boolean = {
    val solver = SolverFactory.newLight()
    solver.setKeepSolverHot(false)
    solver.newVar(expr.nVars)
    solver.setExpectedNumberOfClauses(expr.size)
    val v = new Vec[IVecInt]
    expr foreach ((l) => v.push(new VecInt(l.toArray)))
    try {
      solver.addAllClauses(v)
      solver.isSatisfiable
    } catch {
      case e: ContradictionException => false
    }
  }
}
