package de.feu.shapley.core.parsers

import concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.async.Async._

/**
  * Attempts to read a [[ParseResult]] from an input [[String]].
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
object FormulaParser {

  /**
    * Attempt to parse a single [[String]]
    * @param formulaStr the [[String]] representation of the [[de.feu.shapley.core.types.PropositionalTypes.Formula]] that should be parsed
    * @param ctx a fixed mapping from variable strings to [[Int]] values that is to be honoured by the parser
    * @return result of the asynchronous parsing process
    */
  def parse(formulaStr: String, ctx: ParseContext = emptyContext): Future[ParseResult] = Parboiled2Parser(formulaStr, ctx)


  /**
    * Attempt to parse a collection of [[String]]
    * @param formulaStrs the [[String]] representations of the [[de.feu.shapley.core.types.PropositionalTypes.Formula]] that should be parsed
    * @param ctx a fixed initial mapping from variable strings to [[Int]] values that is to be honoured by the parser
    * @return result of the asynchronous parsing process in the same ordering as the input [[String]]s
    */
  def parseAll(formulaStrs: List[String], ctx: ParseContext = emptyContext): Future[List[ParseResult]] = {
    async {
      formulaStrs match {
        case hd :: tl =>
          val res = await {
            parse(hd, ctx)
          }
          res match {
            case a@(ParseSuccess(_, _, _) | ParseFailure(_, _, _)) =>
              res :: await {
                parseAll(tl, a.ctx)
              }
          }
        case List() => List()
      }
    }
  }

}