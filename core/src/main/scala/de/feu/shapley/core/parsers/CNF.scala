package de.feu.shapley.core.parsers

import de.feu.shapley.core.parsers.ParserExpressions._
import de.feu.shapley.core.types.PropositionalTypes._

import scala.language.postfixOps

/**
  * Converts a [[Formula]] into its Conjunctive Normal Form in a format that is suitable for
  * use in SAT4J, which is a list of lists of [[Int]]s.
  *
  * @author Alexander Erben
  */
private[core] object CNF {
  def apply(that: ParserExpression, variableMapping: ParseContext = Map()): (Formula, ParseContext) = {
    val normalized = distribute(deMorgan(replaceImplication(replaceContradictionTautology(that))))
    toExpression(normalized, variableMapping)
  }

  private def replaceContradictionTautology(that:ParserExpression): ParserExpression = {
    val continue = replaceContradictionTautology _
    that match {
      case Contradiction() => Atom() * f(Atom())
      case Tautology() => Atom() * Atom()
      case a:Atom => a
      case Not(Not(a)) => continue(a)
      case Not(a) => Not(continue(a))
      case Conjunction(a, b) => Conjunction(continue(a), continue(b))
      case Disjunction(a, b) => Disjunction(continue(a), continue(b))
      case Implication(a, b) => Implication(continue(a), continue(b))
      case Coimplication(a, b) => Coimplication(continue(a), continue(b))
    }
  }

  private def replaceImplication(that:ParserExpression): ParserExpression = {
    val continue = replaceImplication _
    that match {
      case a:Atom => a
      case Not(Not(a)) => continue(a)
      case Not(a) => Not(continue(a))
      case Implication(a, b) => Not(continue(a)) + continue(b)
      case Coimplication(a, b) =>
        val (aR, bR) = (continue(a), continue(b))
        (Not(aR) + bR) * (aR + Not(bR))
      case Conjunction(a, b) => Conjunction(continue(a), continue(b))
      case Disjunction(a, b) => Disjunction(continue(a), continue(b))
      case Contradiction() => throw new IllegalStateException("Contradiction should already be replaced!")
      case Tautology() => throw new IllegalStateException("Tautology should already be replaced!")
    }
  }

  private def deMorgan(that:ParserExpression): ParserExpression = {
    val continue = deMorgan _
    that match {
      case a:Atom => a
      case Not(Not(a)) => continue(a)
      case Not(Conjunction(a, b)) => continue(Not(a) + Not(b))
      case Not(Disjunction(a, b)) => continue(Not(a) * Not(b))
      case Not(a) => Not(continue(a))
      case Conjunction(a, b) => Conjunction(continue(a), continue(b))
      case Disjunction(a, b) => Disjunction(continue(a), continue(b))
      case Contradiction() => throw new IllegalStateException("Contradiction should already be replaced!")
      case Tautology() => throw new IllegalStateException("Tautology should already be replaced!")
      case a:Implication => throw new IllegalStateException("Implication should already be replaced!")
      case a:Coimplication => throw new IllegalStateException("Coimplication should already be replaced!")
    }
  }

  private def distribute(in: ParserExpression): ParserExpression = {
    def continue(that: ParserExpression): ParserExpression = that match {
      case a@Atom(_) => a
      case x@Not(a) => Not(continue(a))
      case Conjunction(a, b) => continue(a) * continue(b)
      case Disjunction(a, Conjunction(b, c)) =>
        val (aR, bR, cR) = (continue(a), continue(b), continue(c))
        (aR + bR) * (aR + cR)
      case Disjunction(Conjunction(a, b), c) =>
        val (aR, bR, cR) = (continue(a), continue(b), continue(c))
        (aR + cR) * (bR + cR)
      case Disjunction(a, b) =>
        continue(a) + continue(b)
      case Contradiction() => throw new IllegalStateException("Contradiction should already be replaced!")
      case Tautology() => throw new IllegalStateException("Tautology should already be replaced!")
      case a:Implication => throw new IllegalStateException("Implication should already be replaced!")
      case a:Coimplication => throw new IllegalStateException("Coimplication should already be replaced!")
      }
    val step = continue(in)
    if (step.equals(in)) step else distribute(step)
  }


  def toExpression(normalized: ParserExpression, context: ParseContext): (Formula, ParseContext) = {
    def extractAtoms(e:ParserExpression): List[Atom] = e match {
      case e:Atom => List(e)
      case _ => e.components flatMap extractAtoms
    }

    val variablesOfExpression = extractAtoms(normalized).map(_.value).toSet
    val newVariables = variablesOfExpression filterNot context.contains toStream
    val predefinedVariables = context.values.toList
    val availableVariableInts = Stream from 1 filter (!predefinedVariables.contains(_))
    val newContext = context ++ newVariables.zip(availableVariableInts).toMap

    def convert(expression:ParserExpression): Formula = expression match {
      case Atom(value) => List(List(newContext(value)))
      case Not(Atom(value)) => List(List(-newContext(value)))
      case Disjunction(a, b) => List(convert(a).flatten ::: convert(b).flatten)
      case Conjunction(a, b) => convert(a) ::: convert(b)
      case e => throw new RuntimeException("Unexpected input: " + e)
    }

    (convert(normalized), newContext)
  }

}