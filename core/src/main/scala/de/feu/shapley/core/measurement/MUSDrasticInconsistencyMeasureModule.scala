package de.feu.shapley.core.measurement

import de.feu.shapley.core.measurement.MinimumInconsistentSubsetMeasureModule.Sat4JMinimumInconsistentSubsetMeasure
import de.feu.shapley.core.types.PropositionalTypes.KnowledgeBase
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * An implementation of the drastic inconsistency measure that is based
  * on calculating minimal-unsatifsfiable subsets via SAT4J
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
object MUSDrasticInconsistencyMeasureModule extends InconsistencyMeasureModule {

  override def bind(K: KnowledgeBase): Future[BoundInconsistencyMeasure] = Future {
    new MUSDrasticInconsistencyMeasure(K)
  }

  override def identify = "MUSDrastic"

  private class MUSDrasticInconsistencyMeasure(val domain: KnowledgeBase) extends BoundInconsistencyMeasure{
    val delegate = new Sat4JMinimumInconsistentSubsetMeasure(domain)
    override def measure(K: KnowledgeBase) = delegate.measure(K) map (result => if (result > 0d) 1d else 0d)
  }

}
