package de.feu.shapley.core

import java.nio.file.Paths

/**
  * Created by erben on 13.05.16.
  */
package object provider {

  // Resolve the user directory
  private val userPath = Paths get (System getProperty "user.home")

  /**
    * The application plugin directory. Might not exist.
    */
  val pluginPath = userPath resolve ".siv"

}
