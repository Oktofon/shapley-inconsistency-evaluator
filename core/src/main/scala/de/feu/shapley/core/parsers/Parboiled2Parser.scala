package de.feu.shapley.core.parsers

import de.feu.shapley.core.parsers.ParserExpressions._
import org.parboiled2._
import shapeless.HList

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scala.language.implicitConversions
import scala.util.{Failure, Success}

/**
  * Parser implementation based on parboiled2
  *
  * @author Alexander Erben (a_erben@outlook.com)
  */
private[parsers] object Parboiled2Parser {
  def apply(input: String, context: ParseContext): Future[ParseResult] = {
    val p = Promise[ParseResult]()
    Future {
      try {
        new Parboiled2Parser(input).InputLine.run() match {
          case Success(exprs) =>
            val (expression, newContext) = CNF(exprs, context)
            p.success(ParseSuccess(expression, input, newContext))
          case Failure(reason) =>
            p.success(ParseFailure(reason, input, context))
        }
      } catch {
        case e:Exception => input + "\n " + e.printStackTrace(); throw e
      }
    }
    p.future
  }
  def applyRaw(input: String) = new Parboiled2Parser(input).InputLine.run()
}
private[parsers] class Parboiled2Parser(val input: ParserInput) extends Parser {

  def InputLine = rule {
    Expression ~ EOI
  }

  def Expression: Rule1[ParserExpression] = rule {
    Term ~ zeroOrMore(
      "->" ~ Term ~> Implication |
        "<->" ~ Term ~> Coimplication
    )
  }

  def Term = rule {
      PossibleDoubleNegation ~ zeroOrMore(
        '*' ~ PossibleDoubleNegation ~> Conjunction
          | '+' ~ PossibleDoubleNegation ~> Disjunction
      )
  }

  def PossibleDoubleNegation = rule {
    zeroOrMore(' ') ~ zeroOrMore("!!") ~ PossibleNegation
  }

  def PossibleNegation = rule {
    zeroOrMore(' ') ~ '!' ~ Factor ~> Not | Factor
  }

  def Factor = rule {
    Var | Parens
  }

  def Parens = rule {
    zeroOrMore(' ') ~ '(' ~ zeroOrMore(' ') ~ Expression ~ zeroOrMore(' ') ~ ')' ~ zeroOrMore(' ')
  }

  def Var = rule {
    zeroOrMore(' ') ~ capture(Letters) ~> Atom ~ zeroOrMore(' ')
  }

  def Letters = rule {
    oneOrMore(CharPredicate.Alpha)
  }

  implicit def ignoreWhitespaceInLiteralStrings(s: String): Rule0 = rule {
    str(s) ~ zeroOrMore(' ')
  }
}