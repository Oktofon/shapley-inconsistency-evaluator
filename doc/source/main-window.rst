Hauptfenster
====================


Im Hauptfenster können Sie die Wissensbasen ihres Projekts bearbeiten,
neue Wissensbasen anlegen und löschen:

.. image:: _images/main-window/window.png

-----------------
Projektverwaltung
-----------------

Auf der Oberseite des Fensters befindet sich das Projektmenü:

.. image:: _images/main-window/p-menu.png

Hier können sie das aktuelle Projekt umbennen, ein bestehendes laden, ein neues anlegen oder das aktuelle Projekt schließen. Neu angelegte oder geladene Projekte werden in einem neuen Fenster angezeigt. Das aktuelle Projekt bleibt geöffnet.

---------------------------
Verwaltung der Wissensbasen
---------------------------

In der Liste auf der linken Seite des Fensters haben Sie einen Überblick über alle Wissensbasen, die Ihr Projekt aktuell enthält:

.. image:: _images/main-window/kb-list.png

Die gewählte Wissensbasis wird im Editor auf der rechten Seite angezeigt.

Über das Menü auf der Oberseite des Fensters können Sie neue Wissensbasen anlegen und die aktuell ausgewählte umbenennen oder löschen:

.. image:: _images/main-window/kb-menu.png

----------------------------
Bearbeitung der Wissensbasen
----------------------------

Auf der rechten Seite des Hauptfenster befindet sich der Editor. Hier können sie die Formeln der Wissensbasis bearbeiten und neue hinzufügen. Jede Zeile im Editor steht dabei für eine Formel der Wissensbasis.
Wenn Sie Änderungen im Editor vornehmen, so sind diese zunächst nicht endgültig. Sie können zwischen verschiedenen Wissensbasen wechseln, ohne die Änderungen zu verlieren. Wenn Sie die Änderungen an einer Wissensbasis dauerhaft übernehmen möchten, so drücken Sie auf den "Speichern"-Knopf in der Werkzeugleiste. Wenn Sie Ihre Änderungen wieder verwerfen möchten, drücken Sie auf "Änderungen verwerfen".
Nicht übernommene Änderungen werden beim nächsten Speichern des Projekts nicht auf dem Dateisystem mit gespeichert. Beim Speichern des Projekts warnt Sie aber das Programm, falls noch ungespeicherte Änderungen existieren.

Von der Werkzeugleiste des Editors aus erreichen Sie die :doc:`syntax-check` und das :doc:`analysis-wizard`.
