Syntax-Prüfung
===================

In diesem Fenster werden Ihnen die Ergebnisse der Syntax-Prüfung
als Tabelle angezeigt. Zu jeder Formel finden sie in der rechten Spalte
das Ergebnis.

.. image:: _images/syntax-check.png
