Analyse-Werkzeug
=====================

Das Analysewerkzeug unterstützt sie bei der Berechnung von Inkonsistenzwerten sowie Shapley-Werten aus Inkonsistenzmaßen. Schritt für Schritt können Sie Einstellungen vornehmen, die Ergebnisse prüfen und schließlich als CSV- oder Excel-Datei exportieren.

-----------------
Auswahl des Modus
-----------------

Im ersten Bildschirm werden Sie dazu aufgefordert, das gewünschte Inkonsistenzmaß zu wählen. Außerdem können Sie bestimmen, ob nur der Inkonsistenzwert für die ganze Wissensbasis berechnet werden soll, oder die Shapley-Werte aller Formeln. Ersteres führt deutlich schneller zu einem Ergebnis, lässt aber keine Rückschlüsse auf die Anteile der Inkonsistenz einzelner Formeln an der Wissensbasis zu.

.. image:: _images/analysis-wizard/mode-select.png

-------------------------------------------------
Ergebnis für die Berechnung des Inkonsistenzwerts
-------------------------------------------------

Im Ergebnisfenster für die Berechnung des Inkonsistenzswerts wird Ihnen neben dem Wert selbst auch die Berechnungsdauer angezeigt. Sie können mit dem "Vorheriger"-Knopf wieder zur Modus-Auswahl zurückkehren.

.. image:: _images/analysis-wizard/inconsistency-measure-result.png

---------------------------------------------
Ergebnis für die Berechnung des Shapley-Werts
---------------------------------------------

In diesem Fenster werden Ihnen die Shapley-Werte aller Formeln der Wissensbasen tabellarisch angezeigt. Sie haben die Möglichkeit, die Berechnungsergebnisse als CSV-Datei oder Excel-Datei abzuspeichern.

.. image:: _images/analysis-wizard/shapley-result.png
