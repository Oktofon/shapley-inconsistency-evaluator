Startbildschirm
===============================================================

.. image:: _images/start.png

Der Startbildschirm ist der Einstiegspunkt in den Shapley-Rechner.
Von hier haben Sie folgende Möglichkeiten:

**Ein neues leeres Projekt anlegen**

Wählen Sie diese Option um ein neues Projekt ohne Wissensbasen anzulegen.

**Ein bestehendes Projekt laden**

Wenn Sie bereits ein Projekt abgespeichert haben, können Sie es mit dieser
Option wieder vom Dateisystem laden. Es öffnet sich ein Dateiselektor,
mit dem Sie ihr Projekt auswählen können.

**Ein Beispielprojekt laden**

Um einen Überblick über die Funktionen des Programms zu erhalten, können
Sie ein Beispielprojekt verwenden. Es enthält eine Reihe vorgefertigter
Wissensbasen, mit denen Sie die Funktionen des Programms testen können.

Jede der Optionen bringt sie in den :doc:`main-window`.
