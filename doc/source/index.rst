.. shapley-inconsistency-evaluator-doc documentation master file, created by
   sphinx-quickstart on Sun May  1 12:10:02 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Willkommen zu den Hilfeseiten!
===============================================================

Inhalt:

.. toctree::
   :maxdepth: 2

   start
   main-window
   analysis-wizard
   syntax-check

* :ref:`search`
