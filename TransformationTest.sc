import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node}

val testData: Elem = <project name="shapley-inconsistency-evaluator-bundle" default="default" basedir="." xmlns:fx="javafx:com.sun.javafx.tools.ant">
  <target name="default">
    <taskdef resource="com/sun/javafx/tools/ant/antlib.xml" uri="javafx:com.sun.javafx.tools.ant" classpath="/Users/erben/IdeaProjects/shapley-inconsistency-evaluator/src/deploy:/Library/Java/JavaVirtualMachines/jdk1.8.0.jdk/Contents/Home/lib/ant-javafx.jar"/>

    <fx:application id="shapley-inconsistency-evaluator-bundle" name="shapley-inconsistency-evaluator-bundle" version="0.10.0" mainClass="de.feu.shapley.app.ui.Main"/>
    <fx:platform id="platform" javafx="" j2se="">


    </fx:platform>
    <fx:jar destfile="/Users/erben/IdeaProjects/shapley-inconsistency-evaluator/target/scala-2.11/shapley-inconsistency-evaluator-bundle_2.11.7-0.10.0/shapley-inconsistency-evaluator-bundle_2.11.7-0.10.0.jar">
      <fx:application refid="shapley-inconsistency-evaluator-bundle"/>
      <fx:platform refid="platform"/>
      <fx:fileset dir="/Users/erben/IdeaProjects/shapley-inconsistency-evaluator/target/scala-2.11/classes"/>
      <fx:resources>
        <fx:fileset dir="/Users/erben/IdeaProjects/shapley-inconsistency-evaluator/target/scala-2.11" includes="lib/*.jar"/>
      </fx:resources>

    </fx:jar>


    <fx:deploy width="800" height="600" embeddedWidth="100%" embeddedHeight="100%" outdir="/Users/erben/IdeaProjects/shapley-inconsistency-evaluator/target/scala-2.11/shapley-inconsistency-evaluator-bundle_2.11.7-0.10.0" outfile="shapley-inconsistency-evaluator-bundle_2.11.7-0.10.0" placeholderId="javafx" nativeBundles="dmg" verbose="false">
      <fx:platform refid="platform"/>
      <fx:application refid="shapley-inconsistency-evaluator-bundle"/>
      <fx:info vendor="Unknown" title="Shapley Inconsistency Evaluator" category="" description="" copyright="" license=""></fx:info>
      <fx:resources>
        <fx:fileset dir="/Users/erben/IdeaProjects/shapley-inconsistency-evaluator/target/scala-2.11/shapley-inconsistency-evaluator-bundle_2.11.7-0.10.0" includes="shapley-inconsistency-evaluator-bundle_2.11.7-0.10.0.jar"/>
        <fx:fileset dir="/Users/erben/IdeaProjects/shapley-inconsistency-evaluator/target/scala-2.11" includes="lib/*.jar"/>
      </fx:resources>
      <fx:permissions elevated="false" cacheCertificates="false"/>

    </fx:deploy>
  </target>
</project>
val addNameRule = new RewriteRule {
  override def transform(n: Node): Seq[Node] = {
    n match {
      case Elem(_, "application", attr, _, _*) =>
          <fx:application refid={attr get "refid"} name="Shapley-Rechner"/>
      case other => other
    }
  }
}

val selectRule = new RewriteRule {
  override def transform(n: Node): Seq[Node] = n match {
    case Elem(_,"deploy",_,_,_*) => new RuleTransformer(addNameRule).apply(n)
    case other => other
  }
}



val transform = new RuleTransformer(selectRule)

transform(testData)