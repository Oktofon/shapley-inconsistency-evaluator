# Shapley Inconsistency Evaluator #

## Synopsis ##

Shapley Inconsistency Evaluator is a collection of algorithms, types
and tools to analyse propositional knowledge bases with regards to their
consistency. It supports arbitrary inconsistency measures in terms of the
definition of Hunter and Konieczny as well as the calculation of the shapley
value with inconsistency measures.
On top on the core logic functionality, the application provides a user
interface with a propositional grammar, syntax check, an analysis wizard
and an export tool for the calculation results.

## Prerequisites ##

To build the application, you need the following tools installed on 
your machine and present on the PATH:

- [SBT 0.13.x](http://www.scala-sbt.org/)
- [Scala 2.11.8](http://www.scala-lang.org/)
- [Oracle Java SE Development Kit 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) from Update 60 onwards with the JavaFX 8 SDK included 
- Optional: [Sphinx 1.4.x](http://www.sphinx-doc.org/) To compile the user documentation

## Build from command line ##

To build the application from command line, simply enter the following command in the
project root directory:
    
    sbt clean package package-javafx
    
This command will also generate distribution packages in the target-folder
of the root directory. Depending on your development machine, this
is a DMG for OSX, an MSI for Microsoft Windows or a .deb and .rpm for linux
machines. The distributions contain a bundled JRE for convenient deployment
on any machine.
A ready-to-run jar is generated as well.

## Import into IDE ##

The source code is organised as a SBT multi-module-project. As such, it can be
imported into IntelliJ IDEA Ultimate with the Scala plugin installed
or into [Scala IDE for eclipse](http://scala-ide.org/).

## Write a plug-in inconsistency measure ##

The system supports a plug-in mechanism to roll your own 
inconsistency measures for use in the application. To use it, you have to 
perform the following steps

### Create a new SBT module including the api-jar ###

Use IntelliJ IDEA, Scala IDE or the sbt command line to create a new sbt
module. If you want to the use the command line, you can do as follows:

- Create a new folder for your project, e.g.: mkdir myProject and cd myProject
- Create the project folder structure:
        
        mkdir -p src/main/scala (Mac/Linux)
        mkdir src\main\scala (Windows)
        mkdir lib
        
- Create a build.sbt in the root directory of the project, e.g. with the following content

        name := "my-measure"
        version := "1.0"
        scalaVersion := "2.11.8"

- Copy the jar output of the api-module (it is located in the api/target/scala-2.11 directory) into the lib directory of your new project

### Write an implementation of the InconsistencyMeasureModule trait ###

Implement the trait according to your needs. 
To make things easier, the api-module contains all necessary types and traits for
developing custom inconsistency measures.

### Compile your code as a jar ###

When you are done, create a jar containing your compiled implementation,
e.g. via
    
    sbt package

which will then store the generated jar into the project's target/scala-2.11 directory.

### Store the jar in the plugin directory ###
    
Copy the generated jar to the following location

    ${USER_HOME}/.siv
    
The classpath is scanned at runtime for implementations of InconsistencyMeasureModule,
so that is all you have to do to provide your own inconsistency measure.

### Provide translations for your measure ###

The application expects a Java Resource Bundle by the name "plugin" in the following directory

    ${USER_HOME}/.siv/resources
    
If you want to translate your measure, perform the following steps:

- Assign a key to your measure by implementing the identify-function of the InconsistencyMeasureModule trait

- Provide a fallback translation of the key in 
    
        ${USER_HOME}/.siv/resources/plugin.properties

- For each language that you want to support, provide a translation in

        ${USER_HOME}/.siv/resources/plugin_${languageTag}.properties
    
- Example: For German, store the translation in the following file

        ${USER_HOME}/.siv/resources/plugin_de.properties
    
The files must adhere to the Java Properties File-specification. This means that keys and
values as separated with '=' and entries as separated by newline characters.
The application supports UTF-8 as file format even though Java Properties must normally 
be encoded in ISO-8859-1